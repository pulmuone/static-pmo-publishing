(function() {
  // const slideBox3 = document.querySelector('.slide_box_ver3');
  const slideList3 = document.querySelector('.slide_list3');
  const slideContents3 = document.querySelectorAll('.slide_content3');
  // const slideBtnNext3 = document.querySelector('.slide_btn_next3');
  // const slideBtnPrev3 = document.querySelector('.slide_btn_prev3');
  const pagination3 = document.querySelector('.slide_pagination3');
  const slideLen3 = slideContents3.length;
  const slideWidth3 = slideList3.clientWidth;
  const slideSpeed3 = 300;
  const startNum3 = 0;

//   makeClone();
//   function makeClone() {
//     for (var i = 0; i < slideLen3; i++) {
//       var cloneSlide = slideContents3[i].cloneNode(true);
//       cloneSlide.classList.add('clone');
//       slideList3.appendChild(cloneSlide);
//     }

//     let firstChild = slideList3.lastElementChild;
//     let clonedFirst = firstChild.cloneNode(true);
//     slideList3.insertBefore(clonedFirst, slideList3.firstElementChild);
    
//     newWidth();
//   }
  
//  function newWidth() {
//   var currentSlides = document.querySelectorAll('.slide_content3');
//   var newSlideCount = currentSlides.length;
//   slideList3.style.width = slideWidth3 * (newSlideCount) + "px";
//  }

  slideList3.style.width = slideWidth3 * (slideLen3) + "px";

  let pageChild3 = '';
  for (var i = 0; i < slideLen3; i++) {
    pageChild3 += '<li class="dot3';
    pageChild3 += (i === startNum3) ? ' dot_active3' : '';
    pageChild3 += '" data-index="' + i + '"><a href="#"></a></li>';
  }
  pagination3.innerHTML = pageChild3;
  const pageDots3 = document.querySelectorAll('.dot3');

  // slideList3.style.transform = "translate3d(-" + (slideWidth3 * (startNum3 - 1)) + "px, 0px, 0px)";

  let curIndex3 = startNum3;
  let curSlide3 = slideContents3[curIndex3];
  // curSlide3.classList.add('slide_active3');

  function nextMove3() {
    console.log('nextMove3');
    if (curIndex3 <= slideLen3 - 1) {
      slideList3.style.transition = slideSpeed3 + "ms";
      slideList3.style.transform = "translate3d(-" + (slideWidth3 * (curIndex3 + 1)) + "px, 0px, 0px)";
    } 
    // else {
    //   slideList3.style.transform = "translate3d(0px, 0px, 0px)";
    //   curIndex3 = -1;
    // }

    // if (curIndex3 === slideLen3 - 1) {
    //   setTimeout(function() {
    //     slideList3.style.transition = "0ms";
    //     slideList3.style.transform = "translate3d(-" + (slideWidth3) + "px, 0px, 0px)";
    //   }, slideSpeed3);
    //   curIndex3 = -1;
    // }

    // curSlide3.classList.remove('slide_active3');
    pageDots3[(curIndex3 === -1) ? slideLen3 - 1 : curIndex3].classList.remove('dot_active3');
    curSlide3 = slideContents3[++curIndex3];
    // curSlide3.classList.add('slide_active3');
    pageDots3[curIndex3].classList.add('dot_active3');
  }

  function prevMove3() {
    if (curIndex3 >= 0) {
      slideList3.style.transition = slideSpeed3 + "ms";
      slideList3.style.transform = "translate3d(-" + (slideWidth3 * curIndex3) + "px, 0px, 0px)";
    }

    // if (curIndex3 === 0) {
    //   curIndex3 = 0;
    // }

      
    // if (curIndex3 === 0) {
    //   setTimeout(function() {
    //     slideList3.style.transition = "0ms";
    //     slideList3.style.transform = "translate3d(-" + (slideWidth3 * slideLen3) + "px, 0px, 0px)";
    //   }, slideSpeed3);
    //   curIndex3 = slideLen3;
    // }

    // curSlide3.classList.remove('slide_active3');
    pageDots3[(curIndex3 === slideLen3) ? 0 : curIndex3].classList.remove('dot_active3');
    curSlide3 = slideContents3[--curIndex3];
    // curSlide3.classList.add('slide_active3');
    pageDots3[curIndex3].classList.add('dot_active3');
  }

  // let autoPlayStart3 = setInterval(nextMove3, 2000);

  // function autoSlide3() {
  //   clearInterval(autoPlayStart3);
  //   autoPlayStart3 = setInterval(nextMove3, 2000);
  // }
  // function stopSlide3() {
  //   clearInterval(autoPlayStart3);
  // }

  let curDot3;
  Array.prototype.forEach.call(pageDots3, function(dot, i) {
    dot.addEventListener('click', function(e) {
      e.preventDefault();
      curDot3 = document.querySelector('.dot_active3');
      curDot3.classList.remove('dot_active3');
      curDot3 = this;
      this.classList.add('dot_active3');
      curSlide3.classList.remove('slide_active3');
      curIndex3 = Number(this.getAttribute('data-index'));
      curSlide3 = slideContents3[curIndex3];
      curSlide3.classList.add('slide_active3');
      slideList3.style.transition = slideSpeed3 + "ms";
      slideList3.style.transform = "translate3d(-" + (slideWidth3 * (curIndex3 + 1)) + "px, 0px, 0px)";
    });
  });

  let startPoint3 = 0;
  let endPoint3 = 0;
  var stopFunc3 = function(e) {
    e.preventDefault();
    return false;
  };

  var all3 = document.querySelectorAll('*');

  slideList3.addEventListener("mousedown", (e) => {
    console.log("mousedown", e.pageX);
    startPoint3 = e.pageX;
    e.preventDefault();
    slideList3.addEventListener("mousemove", (e) => {
      for (var idx3 in all3) {
        var el3 = all3[idx3];
        if (el3.addEventListener) {
          el3.addEventListener('click', stopFunc3, true);
        }
      }
    });
  });

  slideList3.addEventListener("mouseup", (e) => {
    console.log("mouseup", e.pageX);
    endPoint3 = e.pageX;
    if (startPoint3 < endPoint3) {
      console.log("prev move");
      prevMove3();
    } else if (startPoint3 > endPoint3) {
      console.log("next move");
      nextMove3();
    }

    slideList3.addEventListener("mousemove", (e) => {
      for (var idx3 in all3) {
        var el3 = all3[idx3];
        if (el3.addEventListener) {
          el3.removeEventListener('click', stopFunc3, true);
        }
      }
    });
  });

  slideList3.addEventListener("touchstart", (e) => {
    // stopSlide3();
    console.log("touchstart", e.touches[0].pageX);
    startPoint3 = e.touches[0].pageX;
  });

  slideList3.addEventListener("touchend", (e) => {
    console.log("touchend", e.changedTouches[0].pageX);
    endPoint3 = e.changedTouches[0].pageX;

    var diff3 = startPoint3 - endPoint3;
    console.log("diff3", diff3);
    if (Math.abs(diff3) < 6) {} 
    else if (startPoint3 < endPoint3) {
      console.log("prev move");
      prevMove3();
    } else if (startPoint3 > endPoint3) {
      console.log("next move");
      nextMove3();
    }

  });


  // slideBox3.addEventListener("touchstart", (e) => {
  //   stopSlide3();
  // });
  // slideBox3.addEventListener("touchend", (e) => {
  //   autoSlide3();
  // });

  // slideBox3.addEventListener("mouseenter", (e) => {
  //   e.preventDefault();
  //   stopSlide3();
  // });
  // slideBox3.addEventListener("mouseleave", (e) => {
  //   e.preventDefault();
  //   autoSlide3();
  // });

  // slideBtnNext3.addEventListener("click", () => {
  //   nextMove3();
  //   console.log("nextMove3");
  // });
  // slideBtnPrev3.addEventListener("click", () => {
  //   prevMove3();
  //   console.log("prevMove3");
  // });

})();