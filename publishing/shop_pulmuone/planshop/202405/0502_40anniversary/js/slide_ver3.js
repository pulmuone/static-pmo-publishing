(function () {
  let slideContents3 = document.querySelectorAll('.slide_list3 li');
  let slideLen3 = slideContents3.length;
  let slideList3 = document.querySelector('.slide_list3');

  function makeClone3() {
    for (var i = 0; i < slideLen3; i++) {
      var cloneSlide3 = slideContents3[i].cloneNode(true);
      cloneSlide3.classList.add('clone');
      slideList3.appendChild(cloneSlide3);
    }
    for (var i = slideLen3 - 1; i >= 0; i--) {
      var cloneSlide3 = slideContents3[i].cloneNode(true);
      cloneSlide3.classList.add('clone');
      slideList3.prepend(cloneSlide3);
    }
  }

  makeClone3();
})();

