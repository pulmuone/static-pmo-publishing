(function() {
  const slideBox2 = document.querySelector('.slide_box2');
  const slideList2 = document.querySelector('.slide_list2');
  const slideContents2 = document.querySelectorAll('.slide_content2');
  const slideBtnNext2 = document.querySelector('.slide_btn_next2');
  const slideBtnPrev2 = document.querySelector('.slide_btn_prev2');
  const pagination2 = document.querySelector('.slide_pagination2');
  const slideLen2 = slideContents2.length;
  const slideWidth2 = slideList2.clientWidth;
  const slideSpeed2 = 400;
  const startNum2 = 0;

  slideList2.style.width = slideWidth2 * (slideLen2 + 2) + "px";

  let firstChild = slideList2.firstElementChild;
  let lastChild = slideList2.lastElementChild;
  let clonedFirst = firstChild.cloneNode(true);
  let clonedLast = lastChild.cloneNode(true);
  slideList2.appendChild(clonedFirst);
  slideList2.insertBefore(clonedLast, slideList2.firstElementChild);

  let pageChild2 = '';
  for (var i = 0; i < slideLen2; i++) {
    pageChild2 += '<li class="dot dot2';
    pageChild2 += (i === startNum2) ? ' dot_active2' : '';
    pageChild2 += '" data-index="' + i + '"><a href="#"></a></li>';
  }
  pagination2.innerHTML = pageChild2;
  const pageDots2 = document.querySelectorAll('.dot2');

  slideList2.style.transform = "translate3d(-" + (slideWidth2 * (startNum2 + 1)) + "px, 0px, 0px)";

  let curIndex2 = startNum2;
  let curSlide2 = slideContents2[curIndex2];
  curSlide2.classList.add('slide_activ');

  function nextMove2() {
    if (curIndex2 <= slideLen2 - 1) {
      slideList2.style.transition = slideSpeed2 + "ms";
      slideList2.style.transform = "translate3d(-" + (slideWidth2 * (curIndex2 + 2)) + "px, 0px, 0px)";
    }

    if (curIndex2 === slideLen2 - 1) {
      setTimeout(function() {
        slideList2.style.transition = "0ms";
        slideList2.style.transform = "translate3d(-" + slideWidth2 + "px, 0px, 0px)";
      }, slideSpeed2);
      curIndex2 = -1;
    }
    curSlide2.classList.remove('slide_activ');
    pageDots2[(curIndex2 === -1) ? slideLen2 - 1 : curIndex2].classList.remove('dot_active2');
    curSlide2 = slideContents2[++curIndex2];
    curSlide2.classList.add('slide_activ');
    pageDots2[curIndex2].classList.add('dot_active2');
  }

  function prevMove2() {
    if (curIndex2 >= 0) {
      slideList2.style.transition = slideSpeed2 + "ms";
      slideList2.style.transform = "translate3d(-" + (slideWidth2 * curIndex2) + "px, 0px, 0px)";
    }
    if (curIndex2 === 0) {
      setTimeout(function() {
        slideList2.style.transition = "0ms";
        slideList2.style.transform = "translate3d(-" + (slideWidth2 * slideLen2) + "px, 0px, 0px)";
      }, slideSpeed2);
      curIndex2 = slideLen2;
    }
    curSlide2.classList.remove('slide_activ');
    pageDots2[(curIndex2 === slideLen2) ? 0 : curIndex2].classList.remove('dot_active2');
    curSlide2 = slideContents2[--curIndex2];
    curSlide2.classList.add('slide_activ');
    pageDots2[curIndex2].classList.add('dot_active2');
  }

  let autoPlayStart2 = setInterval(nextMove2, 3000);

  function autoSlide2() {
    clearInterval(autoPlayStart2);
    autoPlayStart2 = setInterval(nextMove2, 3000);
  }

  function stopSlide2() {
    clearInterval(autoPlayStart2);
  }

  slideBox2.addEventListener("touchstart", (e) => {
    stopSlide2();
  });
  slideBox2.addEventListener("touchend", (e) => {
    autoSlide2();
  });

  slideBox2.addEventListener("mouseenter", (e) => {
    e.preventDefault();
    stopSlide2();
  });

  slideBox2.addEventListener("mouseleave", (e) => {
    e.preventDefault();
    autoSlide2();
  });

  slideBtnNext2.addEventListener("click", () => {
    nextMove2();
    console.log("nextMove2");
  });

  slideBtnPrev2.addEventListener("click", () => {
    prevMove2();
    console.log("prevMove2");
  });


  let curDot2;
  Array.prototype.forEach.call(pageDots2, function(dot, i) {
    dot.addEventListener('click', function(e) {
      e.preventDefault();
      curDot2 = document.querySelector('.dot_active2');
      curDot2.classList.remove('dot_active2');
      curDot2 = this;
      this.classList.add('dot_active2');
      curSlide2.classList.remove('slide_activ');
      curIndex2 = Number(this.getAttribute('data-index'));
      curSlide2 = slideContents2[curIndex2];
      curSlide2.classList.add('slide_activ');
      slideList2.style.transition = slideSpeed2 + "ms";
      slideList2.style.transform = "translate3d(-" + (slideWidth2 * (curIndex2 + 1)) + "px, 0px, 0px)";
    });
  });

  let startPoint = 0;
  let endPoint = 0;
  var stopFunc = function(e) {
    e.preventDefault();
    return false;
  };

  var all = document.querySelectorAll('*');

  slideList2.addEventListener("mousedown", (e) => {
    console.log("mousedown", e.pageX);
    stopSlide2();
    startPoint = e.pageX;
    e.preventDefault();
    slideList2.addEventListener("mousemove", () => {
      for (var idx in all) {
        var el = all[idx];
        if (el.addEventListener) {
          el.addEventListener('click', stopFunc, true);
        }
      }
    });
  });

  slideList2.addEventListener("mouseup", (e) => {
    console.log("mouseup", e.pageX);
    endPoint = e.pageX;
    if (startPoint < endPoint) {
      console.log("prev move");
      prevMove2();
    } else if (startPoint > endPoint) {
      console.log("next move");
      nextMove2();
    }

    slideList2.addEventListener("mousemove", () => {
      for (var idx in all) {
        var el = all[idx];
        if (el.addEventListener) {
          el.removeEventListener('click', stopFunc, true);
        }
      }
    });
  });

  slideList2.addEventListener("touchstart", (e) => {
    stopSlide2();
    console.log("touchstart", e.touches[0].pageX);
    startPoint = e.touches[0].pageX;
  });

  slideList2.addEventListener("touchend", (e) => {
    console.log("touchend", e.changedTouches[0].pageX);
    endPoint = e.changedTouches[0].pageX;

    var diff = startPoint - endPoint;
    console.log("diff", diff);
    if (Math.abs(diff) < 6) {} else if (startPoint < endPoint) {
      console.log("prev move");
      prevMove2();
    } else if (startPoint > endPoint) {
      console.log("next move");
      nextMove2();
    }
  });

})();