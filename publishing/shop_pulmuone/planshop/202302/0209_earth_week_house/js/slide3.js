(function() {
  const slideBox3 = document.querySelector('.slide_box3');
  const slideList3 = document.querySelector('.slide_list3');
  const slideContents3 = document.querySelectorAll('.slide_content3');
  const slideBtnNext3 = document.querySelector('.slide_btn_next3');
  const slideBtnPrev3 = document.querySelector('.slide_btn_prev3');
  const pagination3 = document.querySelector('.slide_pagination3');
  const slideLen3 = slideContents3.length;
  const slideWidth3 = slideList3.clientWidth;
  const slideSpeed3 = 400;
  const startNum3 = 0;

  slideList3.style.width = slideWidth3 * (slideLen3 + 2) + "px";

  let firstChild = slideList3.firstElementChild;
  let lastChild = slideList3.lastElementChild;
  let clonedFirst = firstChild.cloneNode(true);
  let clonedLast = lastChild.cloneNode(true);
  slideList3.appendChild(clonedFirst);
  slideList3.insertBefore(clonedLast, slideList3.firstElementChild);

  let pageChild3 = '';
  for (var i = 0; i < slideLen3; i++) {
    pageChild3 += '<li class="dot dot3';
    pageChild3 += (i === startNum3) ? ' dot_active3' : '';
    pageChild3 += '" data-index="' + i + '"><a href="#"></a></li>';
  }
  pagination3.innerHTML = pageChild3;
  const pageDots3 = document.querySelectorAll('.dot3');

  slideList3.style.transform = "translate3d(-" + (slideWidth3 * (startNum3 + 1)) + "px, 0px, 0px)";

  let curIndex3 = startNum3;
  let curSlide3 = slideContents3[curIndex3];
  curSlide3.classList.add('slide_activ');

  function nextMove3() {
    if (curIndex3 <= slideLen3 - 1) {
      slideList3.style.transition = slideSpeed3 + "ms";
      slideList3.style.transform = "translate3d(-" + (slideWidth3 * (curIndex3 + 2)) + "px, 0px, 0px)";
    }

    if (curIndex3 === slideLen3 - 1) {
      setTimeout(function() {
        slideList3.style.transition = "0ms";
        slideList3.style.transform = "translate3d(-" + slideWidth3 + "px, 0px, 0px)";
      }, slideSpeed3);
      curIndex3 = -1;
    }
    curSlide3.classList.remove('slide_activ');
    pageDots3[(curIndex3 === -1) ? slideLen3 - 1 : curIndex3].classList.remove('dot_active3');
    curSlide3 = slideContents3[++curIndex3];
    curSlide3.classList.add('slide_activ');
    pageDots3[curIndex3].classList.add('dot_active3');
  }

  function prevMove3() {
    if (curIndex3 >= 0) {
      slideList3.style.transition = slideSpeed3 + "ms";
      slideList3.style.transform = "translate3d(-" + (slideWidth3 * curIndex3) + "px, 0px, 0px)";
    }
    if (curIndex3 === 0) {
      setTimeout(function() {
        slideList3.style.transition = "0ms";
        slideList3.style.transform = "translate3d(-" + (slideWidth3 * slideLen3) + "px, 0px, 0px)";
      }, slideSpeed3);
      curIndex3 = slideLen3;
    }
    curSlide3.classList.remove('slide_activ');
    pageDots3[(curIndex3 === slideLen3) ? 0 : curIndex3].classList.remove('dot_active3');
    curSlide3 = slideContents3[--curIndex3];
    curSlide3.classList.add('slide_activ');
    pageDots3[curIndex3].classList.add('dot_active3');
  }

  let autoPlayStart3 = setInterval(nextMove3, 3000);

  function autoSlide3() {
    clearInterval(autoPlayStart3);
    autoPlayStart3 = setInterval(nextMove3, 3000);
  }

  function stopSlide3() {
    clearInterval(autoPlayStart3);
  }

  slideBox3.addEventListener("touchstart", (e) => {
    stopSlide3();
  });
  slideBox3.addEventListener("touchend", (e) => {
    autoSlide3();
  });

  slideBox3.addEventListener("mouseenter", (e) => {
    e.preventDefault();
    stopSlide3();
  });

  slideBox3.addEventListener("mouseleave", (e) => {
    e.preventDefault();
    autoSlide3();
  });

  slideBtnNext3.addEventListener("click", () => {
    nextMove3();
    console.log("nextMove3");
  });

  slideBtnPrev3.addEventListener("click", () => {
    prevMove3();
    console.log("prevMove3");
  });


  let curdot3;
  Array.prototype.forEach.call(pageDots3, function(dot, i) {
    dot.addEventListener('click', function(e) {
      e.preventDefault();
      curdot3 = document.querySelector('.dot_active3');
      curdot3.classList.remove('dot_active3');
      curdot3 = this;
      this.classList.add('dot_active3');
      curSlide3.classList.remove('slide_activ');
      curIndex3 = Number(this.getAttribute('data-index'));
      curSlide3 = slideContents3[curIndex3];
      curSlide3.classList.add('slide_activ');
      slideList3.style.transition = slideSpeed3 + "ms";
      slideList3.style.transform = "translate3d(-" + (slideWidth3 * (curIndex3 + 1)) + "px, 0px, 0px)";
    });
  });

  let startPoint = 0;
  let endPoint = 0;
  var stopFunc = function(e) {
    e.preventDefault();
    return false;
  };

  var all = document.querySelectorAll('*');

  slideList3.addEventListener("mousedown", (e) => {
    console.log("mousedown", e.pageX);
    stopSlide3();
    startPoint = e.pageX;
    e.preventDefault();
    slideList3.addEventListener("mousemove", () => {
      for (var idx in all) {
        var el = all[idx];
        if (el.addEventListener) {
          el.addEventListener('click', stopFunc, true);
        }
      }
    });
  });

  slideList3.addEventListener("mouseup", (e) => {
    console.log("mouseup", e.pageX);
    endPoint = e.pageX;
    if (startPoint < endPoint) {
      console.log("prev move");
      prevMove3();
    } else if (startPoint > endPoint) {
      console.log("next move");
      nextMove3();
    }

    slideList3.addEventListener("mousemove", () => {
      for (var idx in all) {
        var el = all[idx];
        if (el.addEventListener) {
          el.removeEventListener('click', stopFunc, true);
        }
      }
    });
  });

  slideList3.addEventListener("touchstart", (e) => {
    stopSlide3();
    console.log("touchstart", e.touches[0].pageX);
    startPoint = e.touches[0].pageX;
  });

  slideList3.addEventListener("touchend", (e) => {
    console.log("touchend", e.changedTouches[0].pageX);
    endPoint = e.changedTouches[0].pageX;

    var diff = startPoint - endPoint;
    console.log("diff", diff);
    if (Math.abs(diff) < 6) {} else if (startPoint < endPoint) {
      console.log("prev move");
      prevMove3();
    } else if (startPoint > endPoint) {
      console.log("next move");
      nextMove3();
    }
  });

})();