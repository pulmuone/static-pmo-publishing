(function() {
  const slideBox4 = document.querySelector('.slide_box4');
  const slideList4 = document.querySelector('.slide_list4');
  const slideContents4 = document.querySelectorAll('.slide_content4');
  const slideBtnNext4 = document.querySelector('.slide_btn_next4');
  const slideBtnPrev4 = document.querySelector('.slide_btn_prev4');
  const pagination4 = document.querySelector('.slide_pagination4');
  const slideLen4 = slideContents4.length;
  const slideWidth4 = slideList4.clientWidth;
  const slideSpeed4 = 400;
  const startNum4 = 0;

  slideList4.style.width = slideWidth4 * (slideLen4 + 2) + "px";

  let firstChild = slideList4.firstElementChild;
  let lastChild = slideList4.lastElementChild;
  let clonedFirst = firstChild.cloneNode(true);
  let clonedLast = lastChild.cloneNode(true);
  slideList4.appendChild(clonedFirst);
  slideList4.insertBefore(clonedLast, slideList4.firstElementChild);

  let pageChild4 = '';
  for (var i = 0; i < slideLen4; i++) {
    pageChild4 += '<li class="dot dot4';
    pageChild4 += (i === startNum4) ? ' dot_active4' : '';
    pageChild4 += '" data-index="' + i + '"><a href="#"></a></li>';
  }
  pagination4.innerHTML = pageChild4;
  const pageDots4 = document.querySelectorAll('.dot4');

  slideList4.style.transform = "translate3d(-" + (slideWidth4 * (startNum4 + 1)) + "px, 0px, 0px)";

  let curIndex4 = startNum4;
  let curSlide4 = slideContents4[curIndex4];
  curSlide4.classList.add('slide_activ');

  function nextMove4() {
    if (curIndex4 <= slideLen4 - 1) {
      slideList4.style.transition = slideSpeed4 + "ms";
      slideList4.style.transform = "translate3d(-" + (slideWidth4 * (curIndex4 + 2)) + "px, 0px, 0px)";
    }

    if (curIndex4 === slideLen4 - 1) {
      setTimeout(function() {
        slideList4.style.transition = "0ms";
        slideList4.style.transform = "translate3d(-" + slideWidth4 + "px, 0px, 0px)";
      }, slideSpeed4);
      curIndex4 = -1;
    }
    curSlide4.classList.remove('slide_activ');
    pageDots4[(curIndex4 === -1) ? slideLen4 - 1 : curIndex4].classList.remove('dot_active4');
    curSlide4 = slideContents4[++curIndex4];
    curSlide4.classList.add('slide_activ');
    pageDots4[curIndex4].classList.add('dot_active4');
  }

  function prevMove4() {
    if (curIndex4 >= 0) {
      slideList4.style.transition = slideSpeed4 + "ms";
      slideList4.style.transform = "translate3d(-" + (slideWidth4 * curIndex4) + "px, 0px, 0px)";
    }
    if (curIndex4 === 0) {
      setTimeout(function() {
        slideList4.style.transition = "0ms";
        slideList4.style.transform = "translate3d(-" + (slideWidth4 * slideLen4) + "px, 0px, 0px)";
      }, slideSpeed4);
      curIndex4 = slideLen4;
    }
    curSlide4.classList.remove('slide_activ');
    pageDots4[(curIndex4 === slideLen4) ? 0 : curIndex4].classList.remove('dot_active4');
    curSlide4 = slideContents4[--curIndex4];
    curSlide4.classList.add('slide_activ');
    pageDots4[curIndex4].classList.add('dot_active4');
  }

  let autoPlayStart4 = setInterval(nextMove4, 3000);

  function autoSlide4() {
    clearInterval(autoPlayStart4);
    autoPlayStart4 = setInterval(nextMove4, 3000);
  }

  function stopSlide4() {
    clearInterval(autoPlayStart4);
  }

  slideBox4.addEventListener("touchstart", (e) => {
    stopSlide4();
  });
  slideBox4.addEventListener("touchend", (e) => {
    autoSlide4();
  });

  slideBox4.addEventListener("mouseenter", (e) => {
    e.preventDefault();
    stopSlide4();
  });

  slideBox4.addEventListener("mouseleave", (e) => {
    e.preventDefault();
    autoSlide4();
  });

  slideBtnNext4.addEventListener("click", () => {
    nextMove4();
    console.log("nextMove4");
  });

  slideBtnPrev4.addEventListener("click", () => {
    prevMove4();
    console.log("prevMove4");
  });


  let curdot4;
  Array.prototype.forEach.call(pageDots4, function(dot, i) {
    dot.addEventListener('click', function(e) {
      e.preventDefault();
      curdot4 = document.querySelector('.dot_active4');
      curdot4.classList.remove('dot_active4');
      curdot4 = this;
      this.classList.add('dot_active4');
      curSlide4.classList.remove('slide_activ');
      curIndex4 = Number(this.getAttribute('data-index'));
      curSlide4 = slideContents4[curIndex4];
      curSlide4.classList.add('slide_activ');
      slideList4.style.transition = slideSpeed4 + "ms";
      slideList4.style.transform = "translate3d(-" + (slideWidth4 * (curIndex4 + 1)) + "px, 0px, 0px)";
    });
  });

  let startPoint = 0;
  let endPoint = 0;
  var stopFunc = function(e) {
    e.preventDefault();
    return false;
  };

  var all = document.querySelectorAll('*');

  slideList4.addEventListener("mousedown", (e) => {
    console.log("mousedown", e.pageX);
    stopSlide4();
    startPoint = e.pageX;
    e.preventDefault();
    slideList4.addEventListener("mousemove", () => {
      for (var idx in all) {
        var el = all[idx];
        if (el.addEventListener) {
          el.addEventListener('click', stopFunc, true);
        }
      }
    });
  });

  slideList4.addEventListener("mouseup", (e) => {
    console.log("mouseup", e.pageX);
    endPoint = e.pageX;
    if (startPoint < endPoint) {
      console.log("prev move");
      prevMove4();
    } else if (startPoint > endPoint) {
      console.log("next move");
      nextMove4();
    }

    slideList4.addEventListener("mousemove", () => {
      for (var idx in all) {
        var el = all[idx];
        if (el.addEventListener) {
          el.removeEventListener('click', stopFunc, true);
        }
      }
    });
  });

  slideList4.addEventListener("touchstart", (e) => {
    stopSlide4();
    console.log("touchstart", e.touches[0].pageX);
    startPoint = e.touches[0].pageX;
  });

  slideList4.addEventListener("touchend", (e) => {
    console.log("touchend", e.changedTouches[0].pageX);
    endPoint = e.changedTouches[0].pageX;

    var diff = startPoint - endPoint;
    console.log("diff", diff);
    if (Math.abs(diff) < 6) {} else if (startPoint < endPoint) {
      console.log("prev move");
      prevMove4();
    } else if (startPoint > endPoint) {
      console.log("next move");
      nextMove4();
    }
  });

})();