(function () {
  const slideBox = document.querySelector('.slide_wrap_ver2');
  const slideList = document.querySelector('.slide_list2');
  const slideContents = document.querySelectorAll('.slide_content2');
  const slideBtnNext = document.querySelector('.slide_btn_next2');
  const slideBtnPrev = document.querySelector('.slide_btn_prev2');
  const pagination2 = document.querySelector('.slide_pagination2');
  const slideLen = slideContents.length;
  let slideWidth = slideList.clientWidth;
  // let slideWidth = window.innerWidth / 2;
  const slideSpeed = 300;
  const startNum = 0;

  makeClone();

  function makeClone() {
    for (var i = 0; i < slideLen; i++) {
      var cloneSlide = slideContents[i].cloneNode(true);
      cloneSlide.classList.add('clone');
      slideList.appendChild(cloneSlide);
    }

    let firstChild = slideList.lastElementChild;
    let clonedFirst = firstChild.cloneNode(true);
    slideList.insertBefore(clonedFirst, slideList.firstElementChild);

    newWidth();
  }

  function newWidth() {
    var currentSlides = document.querySelectorAll('.slide_content2');
    var newSlideCount = currentSlides.length;
    slideList.style.width = slideWidth * (newSlideCount) + "px";
  }

  //리사이즈
  // window.addEventListener("resize", () => {
  //   console.log('slideList width' + slideList.clientWidth);
  //   console.log('innerwidth:' + window.innerWidth);

  //   slideWidth = window.innerWidth / 2;
  //   newWidth();
  // });

  let pageChild = '';
  for (var i = 0; i < slideLen; i++) {
    pageChild += '<li class="dot2';
    pageChild += (i === startNum) ? ' dot_active2' : '';
    pageChild += '" data-index="' + i + '"><a href="#"></a></li>';
  }
  pagination2.innerHTML = pageChild;
  const pageDots2 = document.querySelectorAll('.dot2');

  slideList.style.transform = "translate3d(-" + (slideWidth * (startNum + 1)) + "px, 0px, 0px)";

  let curIndex = startNum;
  let curSlide = slideContents[curIndex];
  curSlide.classList.add('slide_active2');

  function nextMove() {
    console.log('nextMove');
    if (curIndex <= slideLen - 1) {
      slideList.style.transition = slideSpeed + "ms";
      slideList.style.transform = "translate3d(-" + (slideWidth * (curIndex + 2)) + "px, 0px, 0px)";
    }

    if (curIndex === slideLen - 1) {
      setTimeout(function () {
        slideList.style.transition = "0ms";
        slideList.style.transform = "translate3d(-" + (slideWidth) + "px, 0px, 0px)";
      }, slideSpeed);
      curIndex = -1;
    }

    curSlide.classList.remove('slide_active2');
    pageDots2[(curIndex === -1) ? slideLen - 1 : curIndex].classList.remove('dot_active2');
    curSlide = slideContents[++curIndex];
    curSlide.classList.add('slide_active2');
    pageDots2[curIndex].classList.add('dot_active2');
  }

  function prevMove() {
    if (curIndex >= 0) {
      slideList.style.transition = slideSpeed + "ms";
      slideList.style.transform = "translate3d(-" + (slideWidth * curIndex) + "px, 0px, 0px)";
    }
    if (curIndex === 0) {
      setTimeout(function () {
        slideList.style.transition = "0ms";
        slideList.style.transform = "translate3d(-" + (slideWidth * slideLen) + "px, 0px, 0px)";
      }, slideSpeed);
      curIndex = slideLen;
    }

    curSlide.classList.remove('slide_active2');
    pageDots2[(curIndex === slideLen) ? 0 : curIndex].classList.remove('dot_active2');
    curSlide = slideContents[--curIndex];
    curSlide.classList.add('slide_active2');
    pageDots2[curIndex].classList.add('dot_active2');
  }

  let autoPlayStart = setInterval(nextMove, 2000);

  function autoSlide() {
    clearInterval(autoPlayStart);
    autoPlayStart = setInterval(nextMove, 2000);
  }
  function stopSlide() {
    clearInterval(autoPlayStart);
  }

  let curDot2;
  Array.prototype.forEach.call(pageDots2, function (dot, i) {
    dot.addEventListener('click', function (e) {
      e.preventDefault();
      curDot2 = document.querySelector('.dot_active2');
      curDot2.classList.remove('dot_active2');
      curDot2 = this;
      this.classList.add('dot_active2');
      curSlide.classList.remove('slide_active2');
      curIndex = Number(this.getAttribute('data-index'));
      curSlide = slideContents[curIndex];
      curSlide.classList.add('slide_active2');
      slideList.style.transition = slideSpeed + "ms";
      slideList.style.transform = "translate3d(-" + (slideWidth * (curIndex + 1)) + "px, 0px, 0px)";
    });
  });

  let startPoint = 0;
  let endPoint = 0;
  var stopFunc = function (e) {
    e.preventDefault();
    return false;
  };

  var all = document.querySelectorAll('*');

  slideList.addEventListener("mousedown", (e) => {
    console.log("mousedown", e.pageX);
    startPoint = e.pageX;
    e.preventDefault();
    slideList.addEventListener("mousemove", (e) => {
      for (var idx in all) {
        var el = all[idx];
        if (el.addEventListener) {
          el.addEventListener('click', stopFunc, true);
        }
      }
    });
  });

  slideList.addEventListener("mouseup", (e) => {
    console.log("mouseup", e.pageX);
    endPoint = e.pageX;
    if (startPoint < endPoint) {
      console.log("prev move");
      prevMove();
    } else if (startPoint > endPoint) {
      console.log("next move");
      nextMove();
    }

    slideList.addEventListener("mousemove", (e) => {
      for (var idx in all) {
        var el = all[idx];
        if (el.addEventListener) {
          el.removeEventListener('click', stopFunc, true);
        }
      }
    });
  });

  slideList.addEventListener("touchstart", (e) => {
    stopSlide();
    console.log("touchstart", e.touches[0].pageX);
    startPoint = e.touches[0].pageX;
  });

  slideList.addEventListener("touchend", (e) => {
    console.log("touchend", e.changedTouches[0].pageX);
    endPoint = e.changedTouches[0].pageX;

    var diff2 = startPoint - endPoint;
    console.log("diff2", diff2);
    if (Math.abs(diff2) < 6) { }
    else if (startPoint < endPoint) {
      console.log("prev move");
      prevMove();
    } else if (startPoint > endPoint) {
      console.log("next move");
      nextMove();
    }

  });


  slideBox.addEventListener("touchstart", (e) => {
    stopSlide();
  });
  slideBox.addEventListener("touchend", (e) => {
    autoSlide();
  });

  slideBox.addEventListener("mouseenter", (e) => {
    e.preventDefault();
    stopSlide();
  });
  slideBox.addEventListener("mouseleave", (e) => {
    e.preventDefault();
    autoSlide();
  });

  slideBtnNext.addEventListener("click", () => {
    nextMove();
    console.log("nextMove");
  });
  slideBtnPrev.addEventListener("click", () => {
    prevMove();
    console.log("prevMove");
  });

})();
