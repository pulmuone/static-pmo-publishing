(function() {
  const slideList = document.querySelector('.slide_list3');
  const slideContents = document.querySelectorAll('.slide_content3');
  const slideLen = slideContents.length;
  const slideWidth = slideContents[0].clientWidth;
  const slideSpeed = 1000;
  const startNum = 0;


  for (let i = 0; i < slideLen; i++) {
    let cloneSlide = slideContents[i].cloneNode(true);
    cloneSlide.classList.add('clone');
    slideList.appendChild(cloneSlide);
  }

  for (let i = slideLen - 1; i >= 0; i--) {
    let cloneSlide = slideContents[i].cloneNode(true);
    cloneSlide.classList.add('clone');
    slideList.insertBefore(cloneSlide, slideList.firstElementChild);
  }

  const currentSlides = document.querySelectorAll('.slide_content3');
  const newSlideCount = currentSlides.length;
  slideList.style.width = slideWidth * newSlideCount + "px";
  slideList.style.transform = `translate3d(-${slideWidth * slideLen}px, 0px, 0px)`;


  let curIndex = startNum + slideLen;
  let curSlide = currentSlides[curIndex + 2];
  curSlide.classList.add('slide_active3');

  function nextMove() {
    curSlide.classList.remove('slide_active3');
    curIndex++;
    curSlide = currentSlides[curIndex + 2];
    curSlide.classList.add('slide_active3');

    slideList.style.transition = `${slideSpeed}ms`;
    slideList.style.transform = `translate3d(-${slideWidth * curIndex}px, 0px, 0px)`;

    if (curIndex >= slideLen * 2) {
      setTimeout(function() {
        slideList.style.transition = "0ms";
        slideList.style.transform = `translate3d(-${slideWidth * slideLen}px, 0px, 0px)`;
        curIndex = slideLen;
        curSlide.classList.remove('slide_active3');
        curSlide = currentSlides[curIndex + 2];
        curSlide.classList.add('slide_active3');
      }, slideSpeed);
    }
  }

  setInterval(nextMove, 1300);
})();
