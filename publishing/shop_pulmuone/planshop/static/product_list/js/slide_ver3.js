(function () {
  let slideContents3 = document.querySelectorAll('.slide_content3');
  let slideLen3 = slideContents3.length;
  let slideList3 = document.querySelector('.slide_list3');
  let slideWidth3 = slideList3.clientWidth;

  makeClone3();

  function makeClone3() {
    for (var i = 0; i < slideLen3; i++) {
      var cloneSlide3 = slideContents3[i].cloneNode(true);
      cloneSlide3.classList.add('clone');
      slideList3.appendChild(cloneSlide3);
    }
    for (var i = slideLen3 - 1; i >= 0; i--) {
      var cloneSlide3 = slideContents3[i].cloneNode(true);
      cloneSlide3.classList.add('clone');
      slideList3.prepend(cloneSlide3);
    }
    newWidth3()
  }

  function newWidth3() {
    var currentSlides3 = document.querySelectorAll('.slide_content3');
    var newSlideCount3 = currentSlides3.length;

    slideList3.style.width = slideWidth3 * (newSlideCount3) + "px";
  }

  slideList3.style.transform = "translate3d(-" + (slideWidth3 * (startNum3 + 1)) + "px, 0px, 0px)";

  //리사이즈
  window.addEventListener("resize", () => {
    console.log('slideList width' + slideList3.clientWidth);
    console.log('innerwidth:' + window.innerWidth);

    slideWidth3 = window.innerWidth / 2;
    newWidth();
  });
})();