(function() {
  const slideBox = document.querySelector('.slide_box');
  const slideList = document.querySelector('.slide_list');
  const slideContents = document.querySelectorAll('.slide_content');
  const slideBtnNext = document.querySelector('.slide_btn_next');
  const slideBtnPrev = document.querySelector('.slide_btn_prev');
  const pagination = document.querySelector('.slide_pagination');
  const thumb = document.querySelector('.slide_thumb');
  const slideLen = slideContents.length;
  const slideWidth = slideList.clientWidth;
  const slideSpeed = 400;
  const startNum = 0;

  slideList.style.width = slideWidth * (slideLen + 2) + "px";

  let firstChild = slideList.firstElementChild;
  let lastChild = slideList.lastElementChild;
  let clonedFirst = firstChild.cloneNode(true);
  let clonedLast = lastChild.cloneNode(true);
  slideList.appendChild(clonedFirst);
  slideList.insertBefore(clonedLast, slideList.firstElementChild);

  let pageChild = '';
  for (var i = 0; i < slideLen; i++) {
    pageChild += '<li class="dot';
    pageChild += (i === startNum) ? ' dot_active' : '';
    pageChild += '" data-index="' + i + '"><a href="#"></a></li>';
  }
  pagination.innerHTML = pageChild;
  const pageDots = document.querySelectorAll('.dot');

  let imgSrc = slideList.getElementsByTagName('img');
  let thumbChild = '';
  for (var j = 0; j < slideLen; j++) {
    thumbChild += '<li class="thumb';
    thumbChild += (j === startNum) ? ' thumb_active' : '';
    thumbChild += '" data-index="' + j + '"><a href="#">';
    thumbChild += '<img src="' + imgSrc[j + 1].src + '">';
    thumbChild += '</a></li>';
  }

  thumb.innerHTML = thumbChild;
  const pageThumb = document.querySelectorAll('.thumb');

  slideList.style.transform = "translate3d(-" + (slideWidth * (startNum + 1)) + "px, 0px, 0px)";

  let curIndex = startNum;
  let curSlide = slideContents[curIndex];
  curSlide.classList.add('slide_active');

  function nextMove() {
    if (curIndex <= slideLen - 1) {
      slideList.style.transition = slideSpeed + "ms";
      slideList.style.transform = "translate3d(-" + (slideWidth * (curIndex + 2)) + "px, 0px, 0px)";
    }

    if (curIndex === slideLen - 1) {
      setTimeout(function() {
        slideList.style.transition = "0ms";
        slideList.style.transform = "translate3d(-" + slideWidth + "px, 0px, 0px)";
      }, slideSpeed);
      curIndex = -1;
    }
    curSlide.classList.remove('slide_active');
    pageDots[(curIndex === -1) ? slideLen - 1 : curIndex].classList.remove('dot_active');
    pageThumb[(curIndex === -1) ? slideLen - 1 : curIndex].classList.remove('thumb_active');
    curSlide = slideContents[++curIndex];
    curSlide.classList.add('slide_active');
    pageDots[curIndex].classList.add('dot_active');
    pageThumb[curIndex].classList.add('thumb_active');
  }

  function prevMove() {
    if (curIndex >= 0) {
      slideList.style.transition = slideSpeed + "ms";
      slideList.style.transform = "translate3d(-" + (slideWidth * curIndex) + "px, 0px, 0px)";
    }
    if (curIndex === 0) {
      setTimeout(function() {
        slideList.style.transition = "0ms";
        slideList.style.transform = "translate3d(-" + (slideWidth * slideLen) + "px, 0px, 0px)";
      }, slideSpeed);
      curIndex = slideLen;
    }
    curSlide.classList.remove('slide_active');
    pageDots[(curIndex === slideLen) ? 0 : curIndex].classList.remove('dot_active');
    pageThumb[(curIndex === slideLen) ? 0 : curIndex].classList.remove('thumb_active');
    curSlide = slideContents[--curIndex];
    curSlide.classList.add('slide_active');
    pageDots[curIndex].classList.add('dot_active');
    pageThumb[curIndex].classList.add('thumb_active');
  }

  let autoPlayStart = setInterval(nextMove, 3000);

  function autoSlide() {
    clearInterval(autoPlayStart);
    autoPlayStart = setInterval(nextMove, 3000);
  }

  function stopSlide() {
    clearInterval(autoPlayStart);
  }

  slideBox.addEventListener("touchstart", (e) => {
    stopSlide();
  });
  slideBox.addEventListener("touchend", (e) => {
    autoSlide();
  });

  slideBox.addEventListener("mouseenter", (e) => {
    e.preventDefault();
    stopSlide();
  });

  slideBox.addEventListener("mouseleave", (e) => {
    e.preventDefault();
    autoSlide();
  });

  slideBtnNext.addEventListener("click", () => {
    nextMove();
    console.log("nextMove");
  });

  slideBtnPrev.addEventListener("click", () => {
    prevMove();
    console.log("prevMove");
  });


  let curDot;
  Array.prototype.forEach.call(pageDots, function(dot, i) {
    dot.addEventListener('click', function(e) {
      e.preventDefault();
      curDot = document.querySelector('.dot_active');
      curDot.classList.remove('dot_active');
      curDot = this;
      this.classList.add('dot_active');
      curSlide.classList.remove('slide_active');
      curIndex = Number(this.getAttribute('data-index'));
      curSlide = slideContents[curIndex];
      curSlide.classList.add('slide_active');
      slideList.style.transition = slideSpeed + "ms";
      slideList.style.transform = "translate3d(-" + (slideWidth * (curIndex + 1)) + "px, 0px, 0px)";

      curThumb = document.querySelector('.thumb_active');
      curThumb.classList.remove('thumb_active');
      pageThumb[curIndex].classList.add('thumb_active');

    });
  });

  let curThumb;
  Array.prototype.forEach.call(pageThumb, function(thumb, j) {
    thumb.addEventListener('click', function(e) {
      e.preventDefault();
      curThumb = document.querySelector('.thumb_active');
      curThumb.classList.remove('thumb_active');
      curThumb = this;
      this.classList.add('thumb_active');

      curSlide.classList.remove('slide_active');
      curIndex = Number(this.getAttribute('data-index'));
      curSlide = slideContents[curIndex];
      curSlide.classList.add('slide_active');
      slideList.style.transition = slideSpeed + "ms";
      slideList.style.transform = "translate3d(-" + (slideWidth * (curIndex + 1)) + "px, 0px, 0px)";

      curDot = document.querySelector('.dot_active');
      curDot.classList.remove('dot_active');
      pageDots[curIndex].classList.add('dot_active');
    });
  });


  let startPoint = 0;
  let endPoint = 0;
  var stopFunc = function(e) {
    e.preventDefault();
    return false;
  };

  var all = document.querySelectorAll('*');

  slideList.addEventListener("mousedown", (e) => {
    console.log("mousedown", e.pageX);
    stopSlide();
    startPoint = e.pageX;
    e.preventDefault();
    slideList.addEventListener("mousemove", () => {
      for (var idx in all) {
        var el = all[idx];
        if (el.addEventListener) {
          el.addEventListener('click', stopFunc, true);
        }
      }
    });
  });

  slideList.addEventListener("mouseup", (e) => {
    console.log("mouseup", e.pageX);
    endPoint = e.pageX;
    if (startPoint < endPoint) {
      console.log("prev move");
      prevMove();
    } else if (startPoint > endPoint) {
      console.log("next move");
      nextMove();
    }

    slideList.addEventListener("mousemove", () => {
      for (var idx in all) {
        var el = all[idx];
        if (el.addEventListener) {
          el.removeEventListener('click', stopFunc, true);
        }
      }
    });
  });

  slideList.addEventListener("touchstart", (e) => {
    stopSlide();
    console.log("touchstart", e.touches[0].pageX);
    startPoint = e.touches[0].pageX;
  });

  slideList.addEventListener("touchend", (e) => {
    console.log("touchend", e.changedTouches[0].pageX);
    endPoint = e.changedTouches[0].pageX;

    var diff = startPoint - endPoint;
    console.log("diff", diff);
    if (Math.abs(diff) < 6) {} else if (startPoint < endPoint) {
      console.log("prev move");
      prevMove();
    } else if (startPoint > endPoint) {
      console.log("next move");
      nextMove();
    }
  });

})();