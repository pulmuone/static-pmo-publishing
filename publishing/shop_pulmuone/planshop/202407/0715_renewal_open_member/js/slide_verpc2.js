(function () {
  const slideBox2 = document.querySelector('.slide_box_ver2');
  const slideList2 = document.querySelector('.slide_list2');
  const slideContents2 = document.querySelectorAll('.slide_content2');
  const slideBtnNext2 = document.querySelector('.slide_btn_next2');
  const slideBtnPrev2 = document.querySelector('.slide_btn_prev2');
  const slideBtnPlay2 = document.querySelector('.slide_btn_play2');
  const slideBtnStop2 = document.querySelector('.slide_btn_stop2');
  const pagination2 = document.querySelector('.slide_pagination2');
  const slideLen2 = slideContents2.length;
  let slideWidth2 = slideList2.clientWidth / 5;
  // let slideWidth2 = window.innerWidth / 5;
  const slideSpeed2 = 300;
  const startNum2 = 0;

  makeClone();

  function makeClone() {
    for (var i = 0; i < slideLen2; i++) {
      var cloneSlide = slideContents2[i].cloneNode(true);
      cloneSlide.classList.add('clone');
      slideList2.appendChild(cloneSlide);
    }

    let firstChild = slideList2.lastElementChild;
    let clonedFirst = firstChild.cloneNode(true);
    slideList2.insertBefore(clonedFirst, slideList2.firstElementChild);

    newWidth();
  }

  function newWidth() {
    var currentSlides = document.querySelectorAll('.slide_content2');
    var newSlideCount = currentSlides.length;
    slideList2.style.width = slideWidth2 * (newSlideCount) + "px";
  }

  //리사이즈
  // window.addEventListener("resize", () => {
  //   console.log('slideList width' + slideList2.clientWidth);
  //   console.log('innerwidth:' + window.innerWidth);

  //   slideWidth2 = window.innerWidth / 5;
  //   newWidth();
  // });

  let pageChild2 = '';
  for (var i = 0; i < slideLen2; i++) {
    pageChild2 += '<li class="dot2';
    pageChild2 += (i === startNum2) ? ' dot_active2' : '';
    pageChild2 += '" data-index="' + i + '"><a href="#"></a></li>';
  }
  pagination2.innerHTML = pageChild2;
  const pageDots2 = document.querySelectorAll('.dot2');

  slideList2.style.transform = "translate3d(-" + (slideWidth2 * (startNum2 + 1)) + "px, 0px, 0px)";

  let curIndex2 = startNum2;
  let curSlide2 = slideContents2[curIndex2];
  curSlide2.classList.add('slide_active2');

  function nextMove2() {
    console.log('nextMove2');
    if (curIndex2 <= slideLen2 - 1) {
      slideList2.style.transition = slideSpeed2 + "ms";
      slideList2.style.transform = "translate3d(-" + (slideWidth2 * (curIndex2 + 2)) + "px, 0px, 0px)";
    }

    if (curIndex2 === slideLen2 - 1) {
      setTimeout(function () {
        slideList2.style.transition = "0ms";
        slideList2.style.transform = "translate3d(-" + (slideWidth2) + "px, 0px, 0px)";
      }, slideSpeed2);
      curIndex2 = -1;
    }

    curSlide2.classList.remove('slide_active2');
    pageDots2[(curIndex2 === -1) ? slideLen2 - 1 : curIndex2].classList.remove('dot_active2');
    curSlide2 = slideContents2[++curIndex2];
    curSlide2.classList.add('slide_active2');
    pageDots2[curIndex2].classList.add('dot_active2');
  }

  function prevMove2() {
    if (curIndex2 >= 0) {
      slideList2.style.transition = slideSpeed2 + "ms";
      slideList2.style.transform = "translate3d(-" + (slideWidth2 * curIndex2) + "px, 0px, 0px)";
    }
    if (curIndex2 === 0) {
      setTimeout(function () {
        slideList2.style.transition = "0ms";
        slideList2.style.transform = "translate3d(-" + (slideWidth2 * slideLen2) + "px, 0px, 0px)";
      }, slideSpeed2);
      curIndex2 = slideLen2;
    }

    curSlide2.classList.remove('slide_active2');
    pageDots2[(curIndex2 === slideLen2) ? 0 : curIndex2].classList.remove('dot_active2');
    curSlide2 = slideContents2[--curIndex2];
    curSlide2.classList.add('slide_active2');
    pageDots2[curIndex2].classList.add('dot_active2');
  }

  let autoPlayStart2 = setInterval(nextMove2, 2000);

  function autoSlide2() {
    clearInterval(autoPlayStart2);
    autoPlayStart2 = setInterval(nextMove2, 2000);
  }
  function stopSlide2() {
    clearInterval(autoPlayStart2);
  }

  let curDot2;
  Array.prototype.forEach.call(pageDots2, function (dot, i) {
    dot.addEventListener('click', function (e) {
      e.preventDefault();
      curDot2 = document.querySelector('.dot_active2');
      curDot2.classList.remove('dot_active2');
      curDot2 = this;
      this.classList.add('dot_active2');
      curSlide2.classList.remove('slide_active2');
      curIndex2 = Number(this.getAttribute('data-index'));
      curSlide2 = slideContents2[curIndex2];
      curSlide2.classList.add('slide_active2');
      slideList2.style.transition = slideSpeed2 + "ms";
      slideList2.style.transform = "translate3d(-" + (slideWidth2 * (curIndex2 + 1)) + "px, 0px, 0px)";
    });
  });

  let startPoint2 = 0;
  let endPoint2 = 0;
  var stopFunc2 = function (e) {
    e.preventDefault();
    return false;
  };

  var all2 = document.querySelectorAll('*');

  slideList2.addEventListener("mousedown", (e) => {
    console.log("mousedown", e.pageX);
    startPoint2 = e.pageX;
    e.preventDefault();
    slideList2.addEventListener("mousemove", (e) => {
      for (var idx2 in all2) {
        var el2 = all2[idx2];
        if (el2.addEventListener) {
          el2.addEventListener('click', stopFunc2, true);
        }
      }
    });
  });

  slideList2.addEventListener("mouseup", (e) => {
    console.log("mouseup", e.pageX);
    endPoint2 = e.pageX;
    if (startPoint2 < endPoint2) {
      console.log("prev move");
      prevMove2();
    } else if (startPoint2 > endPoint2) {
      console.log("next move");
      nextMove2();
    }

    slideList2.addEventListener("mousemove", (e) => {
      for (var idx2 in all2) {
        var el2 = all2[idx2];
        if (el2.addEventListener) {
          el2.removeEventListener('click', stopFunc2, true);
        }
      }
    });
  });

  slideList2.addEventListener("touchstart", (e) => {
    stopSlide2();
    console.log("touchstart", e.touches[0].pageX);
    startPoint2 = e.touches[0].pageX;
  });

  slideList2.addEventListener("touchend", (e) => {
    console.log("touchend", e.changedTouches[0].pageX);
    endPoint2 = e.changedTouches[0].pageX;

    var diff2 = startPoint2 - endPoint2;
    console.log("diff2", diff2);
    if (Math.abs(diff2) < 6) { }
    else if (startPoint2 < endPoint2) {
      console.log("prev move");
      prevMove2();
    } else if (startPoint2 > endPoint2) {
      console.log("next move");
      nextMove2();
    }

  });

  slideBtnPlay2.addEventListener("click", () => {
    slideBtnPlay2.style.display = "none";
    slideBtnStop2.style.display = "block";

    autoSlide2();
    slideBox2.addEventListener("mouseenter", (e) => {
      e.preventDefault();
      autoSlide2();
    });
    slideBox2.addEventListener("mouseleave", (e) => {
      e.preventDefault();
      autoSlide2();
    });
    slideBtnNext2.addEventListener("click", () => {
      stopSlide2();
    });
    slideBtnNext2.addEventListener("mouseleave", () => {
      autoSlide2();
    });
    slideBtnPrev2.addEventListener("click", () => {
      stopSlide2();
    });
    slideBtnPrev2.addEventListener("mouseleave", () => {
      autoSlide2();
    });
    pagination2.addEventListener("click", () => {
      stopSlide2();
    });
    pagination2.addEventListener("mouseleave", () => {
      autoSlide2();
    });
  });

  slideBtnStop2.addEventListener("click", () => {
    slideBtnStop2.style.display = "none";
    slideBtnPlay2.style.display = "block";

    stopSlide2();
    slideBox2.addEventListener("mouseenter", (e) => {
      e.preventDefault();
      stopSlide2();
    });
    slideBox2.addEventListener("mouseleave", (e) => {
      e.preventDefault();
      stopSlide2();
    });
    slideBtnNext2.addEventListener("click", (e) => {
      e.preventDefault();
      stopSlide2();
    });
    slideBtnNext2.addEventListener("mouseleave", () => {
      stopSlide2();
    });
    slideBtnPrev2.addEventListener("click", (e) => {
      e.preventDefault();
      stopSlide2();
    });
    slideBtnPrev2.addEventListener("mouseleave", () => {
      stopSlide2();
    });
    pagination2.addEventListener("click", () => {
      stopSlide2();
    });
    pagination2.addEventListener("mouseleave", () => {
      stopSlide2();
    });
  });

  slideBox2.addEventListener("touchstart", (e) => {
    stopSlide2();
  });
  slideBox2.addEventListener("touchend", (e) => {
    autoSlide2();
  });

  slideBox2.addEventListener("mouseenter", (e) => {
    e.preventDefault();
    stopSlide2();
  });
  slideBox2.addEventListener("mouseleave", (e) => {
    e.preventDefault();
    autoSlide2();
  });

  slideBtnNext2.addEventListener("click", () => {
    nextMove2();
    console.log("nextMove2");
  });
  slideBtnPrev2.addEventListener("click", () => {
    prevMove2();
    console.log("prevMove2");
  });

})();
