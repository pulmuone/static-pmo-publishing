(function() {
  const slideBox = document.querySelector('.slide_box');
  const slideList = document.querySelector('.slide_list');
  const slideContents = document.querySelectorAll('.slide_content');
  const slideBtnNext = document.querySelector('.slide_btn_next');
  const slideBtnPrev = document.querySelector('.slide_btn_prev');
  const pagination = document.querySelector('.slide_pagination');
  const slideLen = slideContents.length;
  let slideWidth = slideList.clientWidth;
  const slideSpeed = 300;
  const startNum = 0;

  makeClone1();

  function makeClone1() {
    for (var i = 0; i < slideLen; i++) {
      var cloneSlide = slideContents[i].cloneNode(true);
      cloneSlide.classList.add('clone2');
      slideList.appendChild(cloneSlide);
    }

    let firstChild = slideList.lastElementChild;
    let clonedFirst = firstChild.cloneNode(true);
    slideList.insertBefore(clonedFirst, slideList.firstElementChild);
    
    newWidth();
  }
  
function newWidth() {
  var currentSlides = document.querySelectorAll('.slide_content');
  var newSlideCount = currentSlides.length;
  slideList.style.width = slideWidth * (newSlideCount) + "px";
}

  let pageChild = '';
  for (var i = 0; i < slideLen; i++) {
    pageChild += '<li class="dot';
    pageChild += (i === startNum) ? ' dot_active' : '';
    pageChild += '" data-index="' + i + '"><a href="#"></a></li>';
  }
  pagination.innerHTML = pageChild;
  const pageDots = document.querySelectorAll('.dot');

  slideList.style.transform = "translate3d(-" + (slideWidth * (startNum + 1)) + "px, 0px, 0px)";

  let curIndex = startNum;
  let curSlide = slideContents[curIndex];
  curSlide.classList.add('slide_active');

  function nextMove() {
    console.log('nextMove');
    if (curIndex <= slideLen - 1) {
      slideList.style.transition = slideSpeed + "ms";
      slideList.style.transform = "translate3d(-" + (slideWidth * (curIndex + 2)) + "px, 0px, 0px)";
    }

    if (curIndex === slideLen - 1) {
      setTimeout(function() {
        slideList.style.transition = "0ms";
        slideList.style.transform = "translate3d(-" + (slideWidth) + "px, 0px, 0px)";
      }, slideSpeed);
      curIndex = -1;
    }

    curSlide.classList.remove('slide_active');
    pageDots[(curIndex === -1) ? slideLen - 1 : curIndex].classList.remove('dot_active');
    curSlide = slideContents[++curIndex];
    curSlide.classList.add('slide_active');
    pageDots[curIndex].classList.add('dot_active');
  }

  function prevMove() {
    if (curIndex >= 0) {
      slideList.style.transition = slideSpeed + "ms";
      slideList.style.transform = "translate3d(-" + (slideWidth * curIndex) + "px, 0px, 0px)";
    }
    if (curIndex === 0) {
      setTimeout(function() {
        slideList.style.transition = "0ms";
        slideList.style.transform = "translate3d(-" + (slideWidth * slideLen) + "px, 0px, 0px)";
      }, slideSpeed);
      curIndex = slideLen;
    }

    curSlide.classList.remove('slide_active');
    pageDots[(curIndex === slideLen) ? 0 : curIndex].classList.remove('dot_active');
    curSlide = slideContents[--curIndex];
    curSlide.classList.add('slide_active');
    pageDots[curIndex].classList.add('dot_active');
  }

  let autoPlayStart1 = setInterval(nextMove, 2000);

  function autoSlide() {
    clearInterval(autoPlayStart1);
    autoPlayStart1 = setInterval(nextMove, 2000);
  }
  function stopSlide() {
    clearInterval(autoPlayStart1);
  }

  let curDot;
  Array.prototype.forEach.call(pageDots, function(dot, i) {
    dot.addEventListener('click', function(e) {
      e.preventDefault();
      curDot = document.querySelector('.dot_active');
      curDot.classList.remove('dot_active');
      curDot = this;
      this.classList.add('dot_active');
      curSlide.classList.remove('slide_active');
      curIndex = Number(this.getAttribute('data-index'));
      curSlide = slideContents[curIndex];
      curSlide.classList.add('slide_active');
      slideList.style.transition = slideSpeed + "ms";
      slideList.style.transform = "translate3d(-" + (slideWidth * (curIndex + 1)) + "px, 0px, 0px)";
    });
  });

  let startPoint = 0;
  let endPoint = 0;
  var stopFunc = function(e) {
    e.preventDefault();
    return false;
  };

  var all = document.querySelectorAll('*');

  slideList.addEventListener("mousedown", (e) => {
    console.log("mousedown", e.pageX);
    startPoint = e.pageX;
    e.preventDefault();
    slideList.addEventListener("mousemove", (e) => {
      for (var idx in all) {
        var el = all[idx];
        if (el.addEventListener) {
          el.addEventListener('click', stopFunc, true);
        }
      }
    });
  });

  slideList.addEventListener("mouseup", (e) => {
    console.log("mouseup", e.pageX);
    endPoint = e.pageX;
    if (startPoint < endPoint) {
      console.log("prev move");
      prevMove();
    } else if (startPoint > endPoint) {
      console.log("next move");
      nextMove();
    }

    slideList.addEventListener("mousemove", (e) => {
      for (var idx in all) {
        var el = all[idx];
        if (el.addEventListener) {
          el.removeEventListener('click', stopFunc, true);
        }
      }
    });
  });

  slideList.addEventListener("touchstart", (e) => {
    stopSlide();
    console.log("touchstart", e.touches[0].pageX);
    startPoint = e.touches[0].pageX;
  });

  slideList.addEventListener("touchend", (e) => {
    console.log("touchend", e.changedTouches[0].pageX);
    endPoint = e.changedTouches[0].pageX;

    var diff2 = startPoint - endPoint;
    console.log("diff2", diff2);
    if (Math.abs(diff2) < 6) {} 
    else if (startPoint < endPoint) {
      console.log("prev move");
      prevMove();
    } else if (startPoint > endPoint) {
      console.log("next move");
      nextMove();
    }
  });

  slideBox.addEventListener("touchstart", (e) => {
    stopSlide();
  });
  slideBox.addEventListener("touchend", (e) => {
    autoSlide();
  });

  slideBox.addEventListener("mouseenter", (e) => {
    e.preventDefault();
    stopSlide();
  });
  slideBox.addEventListener("mouseleave", (e) => {
    e.preventDefault();
    autoSlide();
  });

  slideBtnNext.addEventListener("click", () => {
    nextMove();
    console.log("nextMove");
  });
  slideBtnPrev.addEventListener("click", () => {
    prevMove();
    console.log("prevMove");
  });

})();
