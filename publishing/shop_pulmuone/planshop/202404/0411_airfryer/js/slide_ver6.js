(function() {slideBtnNext6
  const slideBox6 = document.querySelector('.slide_box_ver6');
  const slideList6 = document.querySelector('.slide_list6');
  const slideContents6 = document.querySelectorAll('.slide_content6');
  const slideBtnNext6 = document.querySelector('.slide_btn_next6');
  const slideBtnPrev6 = document.querySelector('.slide_btn_prev6');
  const pagination6 = document.querySelector('.slide_pagination6');
  const slideLen6 = slideContents6.length;
  let slideWidth6 = slideList6.clientWidth;
  const slideSpeed6 = 300;
  const startNum6 = 0;

  makeClone6();

  function makeClone6() {
    for (var i = 0; i < slideLen6; i++) {
      var cloneSlide6 = slideContents6[i].cloneNode(true);
      cloneSlide6.classList.add('clone5');
      slideList6.appendChild(cloneSlide6);
    }

    let firstChild6 = slideList6.lastElementChild;
    let clonedFirst6 = firstChild6.cloneNode(true);
    slideList6.insertBefore(clonedFirst6, slideList6.firstElementChild);
    
    newWidth6();
  }
  
 function newWidth6() {
  var currentSlides6 = document.querySelectorAll('.slide_content6');
  var newSlideCount6 = currentSlides6.length;
  slideList6.style.width = slideWidth6 * (newSlideCount6) + "px";
 }

  let pageChild6 = '';
  for (var i = 0; i < slideLen6; i++) {
    pageChild6 += '<li class="dot dot6';
    pageChild6 += (i === startNum6) ? ' dot_active6' : '';
    pageChild6 += '" data-index="' + i + '"><a href="#"></a></li>';
  }
  pagination6.innerHTML = pageChild6;
  const pageDots6 = document.querySelectorAll('.dot6');

  slideList6.style.transform = "translate3d(-" + (slideWidth6 * (startNum6 + 1)) + "px, 0px, 0px)";

  let curIndex6 = startNum6;
  let curSlide6 = slideContents6[curIndex6];
  curSlide6.classList.add('slide_active6');

  function nextMove6() {
    console.log('nextMove6');
    if (curIndex6 <= slideLen6 - 1) {
      slideList6.style.transition = slideSpeed6 + "ms";
      slideList6.style.transform = "translate3d(-" + (slideWidth6 * (curIndex6 + 2)) + "px, 0px, 0px)";
    }

    if (curIndex6 === slideLen6 - 1) {
      setTimeout(function() {
        slideList6.style.transition = "0ms";
        slideList6.style.transform = "translate3d(-" + (slideWidth6) + "px, 0px, 0px)";
      }, slideSpeed6);
      curIndex6 = -1;
    }

    curSlide6.classList.remove('slide_active6');
    pageDots6[(curIndex6 === -1) ? slideLen6 - 1 : curIndex6].classList.remove('dot_active6');
    curSlide6 = slideContents6[++curIndex6];
    curSlide6.classList.add('slide_active6');
    pageDots6[curIndex6].classList.add('dot_active6');
  }

  function prevMove6() {
    if (curIndex6 >= 0) {
      slideList6.style.transition = slideSpeed6 + "ms";
      slideList6.style.transform = "translate3d(-" + (slideWidth6 * curIndex6) + "px, 0px, 0px)";
    }
    if (curIndex6 === 0) {
      setTimeout(function() {
        slideList6.style.transition = "0ms";
        slideList6.style.transform = "translate3d(-" + (slideWidth6 * slideLen6) + "px, 0px, 0px)";
      }, slideSpeed6);
      curIndex6 = slideLen6;
    }

    curSlide6.classList.remove('slide_active6');
    pageDots6[(curIndex6 === slideLen6) ? 0 : curIndex6].classList.remove('dot_active6');
    curSlide6 = slideContents6[--curIndex6];
    curSlide6.classList.add('slide_active6');
    pageDots6[curIndex6].classList.add('dot_active6');
  }

  let autoPlayStart6 = setInterval(nextMove6, 2000);

  function autoSlide6() {
    clearInterval(autoPlayStart6);
    autoPlayStart6 = setInterval(nextMove6, 2000);
  }
  function stopSlide6() {
    clearInterval(autoPlayStart6);
  }

  let curDot6;
  Array.prototype.forEach.call(pageDots6, function(dot, i) {
    dot.addEventListener('click', function(e) {
      e.preventDefault();
      curDot6 = document.querySelector('.dot_active6');
      curDot6.classList.remove('dot_active6');
      curDot6 = this;
      this.classList.add('dot_active6');
      curSlide6.classList.remove('slide_active6');
      curIndex6 = Number(this.getAttribute('data-index'));
      curSlide6 = slideContents6[curIndex6];
      curSlide6.classList.add('slide_active6');
      slideList6.style.transition = slideSpeed6 + "ms";
      slideList6.style.transform = "translate3d(-" + (slideWidth6 * (curIndex6 + 1)) + "px, 0px, 0px)";
    });
  });

  let startPoint6 = 0;
  let endPoint6 = 0;
  var stopFunc6 = function(e) {
    e.preventDefault();
    return false;
  };

  var all6 = document.querySelectorAll('*');

  slideList6.addEventListener("mousedown", (e) => {
    console.log("mousedown", e.pageX);
    startPoint6 = e.pageX;
    e.preventDefault();
    slideList6.addEventListener("mousemove", (e) => {
      for (var idx6 in all6) {
        var el6 = all6[idx6];
        if (el6.addEventListener) {
          el6.addEventListener('click', stopFunc6, true);
        }
      }
    });
  });

  slideList6.addEventListener("mouseup", (e) => {
    console.log("mouseup", e.pageX);
    endPoint6 = e.pageX;
    if (startPoint6 < endPoint6) {
      console.log("prev move");
      prevMove6();
    } else if (startPoint6 > endPoint6) {
      console.log("next move");
      nextMove6();
    }

    slideList6.addEventListener("mousemove", (e) => {
      for (var idx6 in all6) {
        var el6 = all6[idx6];
        if (el6.addEventListener) {
          el6.removeEventListener('click', stopFunc6, true);
        }
      }
    });
  });

  slideList6.addEventListener("touchstart", (e) => {
    stopSlide6();
    console.log("touchstart", e.touches[0].pageX);
    startPoint6 = e.touches[0].pageX;
  });

  slideList6.addEventListener("touchend", (e) => {
    console.log("touchend", e.changedTouches[0].pageX);
    endPoint6 = e.changedTouches[0].pageX;

    var diff6 = startPoint6 - endPoint6;
    console.log("diff6", diff6);
    if (Math.abs(diff6) < 6) {} 
    else if (startPoint6 < endPoint6) {
      console.log("prev move");
      prevMove6();
    } else if (startPoint6 > endPoint6) {
      console.log("next move");
      nextMove6();
    }

  });


  slideBox6.addEventListener("touchstart", (e) => {
    stopSlide6();
  });
  slideBox6.addEventListener("touchend", (e) => {
    autoSlide6();
  });

  slideBox6.addEventListener("mouseenter", (e) => {
    e.preventDefault();
    stopSlide6();
  });
  slideBox6.addEventListener("mouseleave", (e) => {
    e.preventDefault();
    autoSlide6();
  });

  slideBtnNext6.addEventListener("click", () => {
    nextMove6();
    console.log("nextMove6");
  });
  slideBtnPrev6.addEventListener("click", () => {
    prevMove6();
    console.log("prevMove6");
  });

})();
