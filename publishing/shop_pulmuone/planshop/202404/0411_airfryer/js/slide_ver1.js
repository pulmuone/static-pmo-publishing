(function() {
  const slideBox1 = document.querySelector('.slide_box_ver1');
  const slideList1 = document.querySelector('.slide_list1');
  const slideContents1 = document.querySelectorAll('.slide_content1');
  const slideBtnNext1 = document.querySelector('.slide_btn_next1');
  const slideBtnPrev1 = document.querySelector('.slide_btn_prev1');
  const pagination1 = document.querySelector('.slide_pagination1');
  const slideLen1 = slideContents1.length;
  let slideWidth1 = slideList1.clientWidth;
  const slideSpeed1 = 300;
  const startNum1 = 0;

  makeClone1();

  function makeClone1() {
    for (var i = 0; i < slideLen1; i++) {
      var cloneSlide = slideContents1[i].cloneNode(true);
      cloneSlide.classList.add('clone2');
      slideList1.appendChild(cloneSlide);
    }

    let firstChild = slideList1.lastElementChild;
    let clonedFirst = firstChild.cloneNode(true);
    slideList1.insertBefore(clonedFirst, slideList1.firstElementChild);
    
    newWidth1();
  }
  
 function newWidth1() {
  var currentSlides = document.querySelectorAll('.slide_content1');
  var newSlideCount = currentSlides.length;
  slideList1.style.width = slideWidth1 * (newSlideCount) + "px";
 }

  let pageChild1 = '';
  for (var i = 0; i < slideLen1; i++) {
    pageChild1 += '<li class="dot dot1';
    pageChild1 += (i === startNum1) ? ' dot_active1' : '';
    pageChild1 += '" data-index="' + i + '"><a href="#"></a></li>';
  }
  pagination1.innerHTML = pageChild1;
  const pageDots1 = document.querySelectorAll('.dot1');

  slideList1.style.transform = "translate3d(-" + (slideWidth1 * (startNum1 + 1)) + "px, 0px, 0px)";

  let curIndex1 = startNum1;
  let curSlide1 = slideContents1[curIndex1];
  curSlide1.classList.add('slide_active1');

  function nextMove1() {
    console.log('nextMove1');
    if (curIndex1 <= slideLen1 - 1) {
      slideList1.style.transition = slideSpeed1 + "ms";
      slideList1.style.transform = "translate3d(-" + (slideWidth1 * (curIndex1 + 2)) + "px, 0px, 0px)";
    }

    if (curIndex1 === slideLen1 - 1) {
      setTimeout(function() {
        slideList1.style.transition = "0ms";
        slideList1.style.transform = "translate3d(-" + (slideWidth1) + "px, 0px, 0px)";
      }, slideSpeed1);
      curIndex1 = -1;
    }

    curSlide1.classList.remove('slide_active1');
    pageDots1[(curIndex1 === -1) ? slideLen1 - 1 : curIndex1].classList.remove('dot_active1');
    curSlide1 = slideContents1[++curIndex1];
    curSlide1.classList.add('slide_active1');
    pageDots1[curIndex1].classList.add('dot_active1');
  }

  function prevMove1() {
    if (curIndex1 >= 0) {
      slideList1.style.transition = slideSpeed1 + "ms";
      slideList1.style.transform = "translate3d(-" + (slideWidth1 * curIndex1) + "px, 0px, 0px)";
    }
    if (curIndex1 === 0) {
      setTimeout(function() {
        slideList1.style.transition = "0ms";
        slideList1.style.transform = "translate3d(-" + (slideWidth1 * slideLen1) + "px, 0px, 0px)";
      }, slideSpeed1);
      curIndex1 = slideLen1;
    }

    curSlide1.classList.remove('slide_active1');
    pageDots1[(curIndex1 === slideLen1) ? 0 : curIndex1].classList.remove('dot_active1');
    curSlide1 = slideContents1[--curIndex1];
    curSlide1.classList.add('slide_active1');
    pageDots1[curIndex1].classList.add('dot_active1');
  }

  let autoPlayStart1 = setInterval(nextMove1, 2000);

  function autoSlide1() {
    clearInterval(autoPlayStart1);
    autoPlayStart1 = setInterval(nextMove1, 2000);
  }
  function stopSlide1() {
    clearInterval(autoPlayStart1);
  }

  let curDot1;
  Array.prototype.forEach.call(pageDots1, function(dot, i) {
    dot.addEventListener('click', function(e) {
      e.preventDefault();
      curDot1 = document.querySelector('.dot_active1');
      curDot1.classList.remove('dot_active1');
      curDot1 = this;
      this.classList.add('dot_active1');
      curSlide1.classList.remove('slide_active1');
      curIndex1 = Number(this.getAttribute('data-index'));
      curSlide1 = slideContents1[curIndex1];
      curSlide1.classList.add('slide_active1');
      slideList1.style.transition = slideSpeed1 + "ms";
      slideList1.style.transform = "translate3d(-" + (slideWidth1 * (curIndex1 + 1)) + "px, 0px, 0px)";
    });
  });

  let startPoint1 = 0;
  let endPoint1 = 0;
  var stopFunc2 = function(e) {
    e.preventDefault();
    return false;
  };

  var all1 = document.querySelectorAll('*');

  slideList1.addEventListener("mousedown", (e) => {
    console.log("mousedown", e.pageX);
    startPoint1 = e.pageX;
    e.preventDefault();
    slideList1.addEventListener("mousemove", (e) => {
      for (var idx1 in all1) {
        var el1 = all1[idx1];
        if (el1.addEventListener) {
          el1.addEventListener('click', stopFunc2, true);
        }
      }
    });
  });

  slideList1.addEventListener("mouseup", (e) => {
    console.log("mouseup", e.pageX);
    endPoint1 = e.pageX;
    if (startPoint1 < endPoint1) {
      console.log("prev move");
      prevMove1();
    } else if (startPoint1 > endPoint1) {
      console.log("next move");
      nextMove1();
    }

    slideList1.addEventListener("mousemove", (e) => {
      for (var idx1 in all1) {
        var el1 = all1[idx1];
        if (el1.addEventListener) {
          el1.removeEventListener('click', stopFunc2, true);
        }
      }
    });
  });

  slideList1.addEventListener("touchstart", (e) => {
    stopSlide1();
    console.log("touchstart", e.touches[0].pageX);
    startPoint1 = e.touches[0].pageX;
  });

  slideList1.addEventListener("touchend", (e) => {
    console.log("touchend", e.changedTouches[0].pageX);
    endPoint1 = e.changedTouches[0].pageX;

    var diff2 = startPoint1 - endPoint1;
    console.log("diff2", diff2);
    if (Math.abs(diff2) < 6) {} 
    else if (startPoint1 < endPoint1) {
      console.log("prev move");
      prevMove1();
    } else if (startPoint1 > endPoint1) {
      console.log("next move");
      nextMove1();
    }
  });

  slideBox1.addEventListener("touchstart", (e) => {
    stopSlide1();
  });
  slideBox1.addEventListener("touchend", (e) => {
    autoSlide1();
  });

  slideBox1.addEventListener("mouseenter", (e) => {
    e.preventDefault();
    stopSlide1();
  });
  slideBox1.addEventListener("mouseleave", (e) => {
    e.preventDefault();
    autoSlide1();
  });

  slideBtnNext1.addEventListener("click", () => {
    nextMove1();
    console.log("nextMove1");
  });
  slideBtnPrev1.addEventListener("click", () => {
    prevMove1();
    console.log("prevMove1");
  });





})();
