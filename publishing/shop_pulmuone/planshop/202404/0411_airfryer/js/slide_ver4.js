(function() {
  const slideBox4 = document.querySelector('.slide_box_ver4');
  const slideList4 = document.querySelector('.slide_list4');
  const slideContents4 = document.querySelectorAll('.slide_content4');
  const slideBtnNext4 = document.querySelector('.slide_btn_next4');
  const slideBtnPrev4 = document.querySelector('.slide_btn_prev4');
  const pagination4 = document.querySelector('.slide_pagination4');
  const slideLen4 = slideContents4.length;
  let slideWidth4 = slideList4.clientWidth;
  const slideSpeed4 = 300;
  const startNum4 = 0;

  makeClone4();

  function makeClone4() {
    for (var i = 0; i < slideLen4; i++) {
      var cloneSlide4 = slideContents4[i].cloneNode(true);
      cloneSlide4.classList.add('clone4');
      slideList4.appendChild(cloneSlide4);
    }

    let firstChild4 = slideList4.lastElementChild;
    let clonedFirst4 = firstChild4.cloneNode(true);
    slideList4.insertBefore(clonedFirst4, slideList4.firstElementChild);
    
    newWidth4();
  }
  
 function newWidth4() {
  var currentSlides = document.querySelectorAll('.slide_content4');
  var newSlideCount = currentSlides.length;
  slideList4.style.width = slideWidth4 * (newSlideCount) + "px";
 }

  let pageChild4 = '';
  for (var i = 0; i < slideLen4; i++) {
    pageChild4 += '<li class="dot dot4';
    pageChild4 += (i === startNum4) ? ' dot_active4' : '';
    pageChild4 += '" data-index="' + i + '"><a href="#"></a></li>';
  }
  pagination4.innerHTML = pageChild4;
  const pageDots4 = document.querySelectorAll('.dot4');

  slideList4.style.transform = "translate3d(-" + (slideWidth4 * (startNum4 + 1)) + "px, 0px, 0px)";

  let curIndex4 = startNum4;
  let curSlide4 = slideContents4[curIndex4];
  curSlide4.classList.add('slide_active4');

  function nextMove4() {
    console.log('nextMove4');
    if (curIndex4 <= slideLen4 - 1) {
      slideList4.style.transition = slideSpeed4 + "ms";
      slideList4.style.transform = "translate3d(-" + (slideWidth4 * (curIndex4 + 2)) + "px, 0px, 0px)";
    }

    if (curIndex4 === slideLen4 - 1) {
      setTimeout(function() {
        slideList4.style.transition = "0ms";
        slideList4.style.transform = "translate3d(-" + (slideWidth4) + "px, 0px, 0px)";
      }, slideSpeed4);
      curIndex4 = -1;
    }

    curSlide4.classList.remove('slide_active4');
    pageDots4[(curIndex4 === -1) ? slideLen4 - 1 : curIndex4].classList.remove('dot_active4');
    curSlide4 = slideContents4[++curIndex4];
    curSlide4.classList.add('slide_active4');
    pageDots4[curIndex4].classList.add('dot_active4');
  }

  function prevMove4() {
    if (curIndex4 >= 0) {
      slideList4.style.transition = slideSpeed4 + "ms";
      slideList4.style.transform = "translate3d(-" + (slideWidth4 * curIndex4) + "px, 0px, 0px)";
    }
    if (curIndex4 === 0) {
      setTimeout(function() {
        slideList4.style.transition = "0ms";
        slideList4.style.transform = "translate3d(-" + (slideWidth4 * slideLen4) + "px, 0px, 0px)";
      }, slideSpeed4);
      curIndex4 = slideLen4;
    }

    curSlide4.classList.remove('slide_active4');
    pageDots4[(curIndex4 === slideLen4) ? 0 : curIndex4].classList.remove('dot_active4');
    curSlide4 = slideContents4[--curIndex4];
    curSlide4.classList.add('slide_active4');
    pageDots4[curIndex4].classList.add('dot_active4');
  }

  let autoPlayStart4 = setInterval(nextMove4, 2000);

  function autoSlide4() {
    clearInterval(autoPlayStart4);
    autoPlayStart4 = setInterval(nextMove4, 2000);
  }
  function stopSlide4() {
    clearInterval(autoPlayStart4);
  }

  let curDot4;
  Array.prototype.forEach.call(pageDots4, function(dot, i) {
    dot.addEventListener('click', function(e) {
      e.preventDefault();
      curDot4 = document.querySelector('.dot_active4');
      curDot4.classList.remove('dot_active4');
      curDot4 = this;
      this.classList.add('dot_active4');
      curSlide4.classList.remove('slide_active4');
      curIndex4 = Number(this.getAttribute('data-index'));
      curSlide4 = slideContents4[curIndex4];
      curSlide4.classList.add('slide_active4');
      slideList4.style.transition = slideSpeed4 + "ms";
      slideList4.style.transform = "translate3d(-" + (slideWidth4 * (curIndex4 + 1)) + "px, 0px, 0px)";
    });
  });

  let startPoint4 = 0;
  let endPoint4 = 0;
  var stopFunc4 = function(e) {
    e.preventDefault();
    return false;
  };

  var all4 = document.querySelectorAll('*');

  slideList4.addEventListener("mousedown", (e) => {
    console.log("mousedown", e.pageX);
    startPoint4 = e.pageX;
    e.preventDefault();
    slideList4.addEventListener("mousemove", (e) => {
      for (var idx4 in all4) {
        var el4 = all4[idx4];
        if (el4.addEventListener) {
          el4.addEventListener('click', stopFunc4, true);
        }
      }
    });
  });

  slideList4.addEventListener("mouseup", (e) => {
    console.log("mouseup", e.pageX);
    endPoint4 = e.pageX;
    if (startPoint4 < endPoint4) {
      console.log("prev move");
      prevMove4();
    } else if (startPoint4 > endPoint4) {
      console.log("next move");
      nextMove4();
    }

    slideList4.addEventListener("mousemove", (e) => {
      for (var idx4 in all4) {
        var el4 = all4[idx4];
        if (el4.addEventListener) {
          el4.removeEventListener('click', stopFunc4, true);
        }
      }
    });
  });

  slideList4.addEventListener("touchstart", (e) => {
    stopSlide4();
    console.log("touchstart", e.touches[0].pageX);
    startPoint4 = e.touches[0].pageX;
  });

  slideList4.addEventListener("touchend", (e) => {
    console.log("touchend", e.changedTouches[0].pageX);
    endPoint4 = e.changedTouches[0].pageX;

    var diff4 = startPoint4 - endPoint4;
    console.log("diff4", diff4);
    if (Math.abs(diff4) < 6) {} 
    else if (startPoint4 < endPoint4) {
      console.log("prev move");
      prevMove4();
    } else if (startPoint4 > endPoint4) {
      console.log("next move");
      nextMove4();
    }

  });


  slideBox4.addEventListener("touchstart", (e) => {
    stopSlide4();
  });
  slideBox4.addEventListener("touchend", (e) => {
    autoSlide4();
  });

  slideBox4.addEventListener("mouseenter", (e) => {
    e.preventDefault();
    stopSlide4();
  });
  slideBox4.addEventListener("mouseleave", (e) => {
    e.preventDefault();
    autoSlide4();
  });

  slideBtnNext4.addEventListener("click", () => {
    nextMove4();
    console.log("nextMove4");
  });
  slideBtnPrev4.addEventListener("click", () => {
    prevMove4();
    console.log("prevMove4");
  });

})();
