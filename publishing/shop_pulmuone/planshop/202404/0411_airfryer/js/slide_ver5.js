(function() {
  const slideBox5 = document.querySelector('.slide_box_ver5');
  const slideList5 = document.querySelector('.slide_list5');
  const slideContents5 = document.querySelectorAll('.slide_content5');
  const slideBtnNext5 = document.querySelector('.slide_btn_next5');
  const slideBtnPrev5 = document.querySelector('.slide_btn_prev5');
  const pagination5 = document.querySelector('.slide_pagination5');
  const slideLen5 = slideContents5.length;
  let slideWidth5 = slideList5.clientWidth;
  const slideSpeed5 = 300;
  const startNum5 = 0;

  makeClone5();

  function makeClone5() {
    for (var i = 0; i < slideLen5; i++) {
      var cloneSlide5 = slideContents5[i].cloneNode(true);
      cloneSlide5.classList.add('clone5');
      slideList5.appendChild(cloneSlide5);
    }

    let firstChild5 = slideList5.lastElementChild;
    let clonedFirst5 = firstChild5.cloneNode(true);
    slideList5.insertBefore(clonedFirst5, slideList5.firstElementChild);
    
    newWidth5();
  }
  
 function newWidth5() {
  var currentSlides5 = document.querySelectorAll('.slide_content5');
  var newSlideCount5 = currentSlides5.length;
  slideList5.style.width = slideWidth5 * (newSlideCount5) + "px";
 }

  let pageChild5 = '';
  for (var i = 0; i < slideLen5; i++) {
    pageChild5 += '<li class="dot dot5';
    pageChild5 += (i === startNum5) ? ' dot_active5' : '';
    pageChild5 += '" data-index="' + i + '"><a href="#"></a></li>';
  }
  pagination5.innerHTML = pageChild5;
  const pageDots5 = document.querySelectorAll('.dot5');

  slideList5.style.transform = "translate3d(-" + (slideWidth5 * (startNum5 + 1)) + "px, 0px, 0px)";

  let curIndex5 = startNum5;
  let curSlide5 = slideContents5[curIndex5];
  curSlide5.classList.add('slide_active5');

  function nextMove5() {
    console.log('nextMove5');
    if (curIndex5 <= slideLen5 - 1) {
      slideList5.style.transition = slideSpeed5 + "ms";
      slideList5.style.transform = "translate3d(-" + (slideWidth5 * (curIndex5 + 2)) + "px, 0px, 0px)";
    }

    if (curIndex5 === slideLen5 - 1) {
      setTimeout(function() {
        slideList5.style.transition = "0ms";
        slideList5.style.transform = "translate3d(-" + (slideWidth5) + "px, 0px, 0px)";
      }, slideSpeed5);
      curIndex5 = -1;
    }

    curSlide5.classList.remove('slide_active5');
    pageDots5[(curIndex5 === -1) ? slideLen5 - 1 : curIndex5].classList.remove('dot_active5');
    curSlide5 = slideContents5[++curIndex5];
    curSlide5.classList.add('slide_active5');
    pageDots5[curIndex5].classList.add('dot_active5');
  }

  function prevMove5() {
    if (curIndex5 >= 0) {
      slideList5.style.transition = slideSpeed5 + "ms";
      slideList5.style.transform = "translate3d(-" + (slideWidth5 * curIndex5) + "px, 0px, 0px)";
    }
    if (curIndex5 === 0) {
      setTimeout(function() {
        slideList5.style.transition = "0ms";
        slideList5.style.transform = "translate3d(-" + (slideWidth5 * slideLen5) + "px, 0px, 0px)";
      }, slideSpeed5);
      curIndex5 = slideLen5;
    }

    curSlide5.classList.remove('slide_active5');
    pageDots5[(curIndex5 === slideLen5) ? 0 : curIndex5].classList.remove('dot_active5');
    curSlide5 = slideContents5[--curIndex5];
    curSlide5.classList.add('slide_active5');
    pageDots5[curIndex5].classList.add('dot_active5');
  }

  let autoPlayStart5 = setInterval(nextMove5, 2000);

  function autoSlide5() {
    clearInterval(autoPlayStart5);
    autoPlayStart5 = setInterval(nextMove5, 2000);
  }
  function stopSlide5() {
    clearInterval(autoPlayStart5);
  }

  let curDot5;
  Array.prototype.forEach.call(pageDots5, function(dot, i) {
    dot.addEventListener('click', function(e) {
      e.preventDefault();
      curDot5 = document.querySelector('.dot_active5');
      curDot5.classList.remove('dot_active5');
      curDot5 = this;
      this.classList.add('dot_active5');
      curSlide5.classList.remove('slide_active5');
      curIndex5 = Number(this.getAttribute('data-index'));
      curSlide5 = slideContents5[curIndex5];
      curSlide5.classList.add('slide_active5');
      slideList5.style.transition = slideSpeed5 + "ms";
      slideList5.style.transform = "translate3d(-" + (slideWidth5 * (curIndex5 + 1)) + "px, 0px, 0px)";
    });
  });

  let startPoint5 = 0;
  let endPoint5 = 0;
  var stopFunc5 = function(e) {
    e.preventDefault();
    return false;
  };

  var all5 = document.querySelectorAll('*');

  slideList5.addEventListener("mousedown", (e) => {
    console.log("mousedown", e.pageX);
    startPoint5 = e.pageX;
    e.preventDefault();
    slideList5.addEventListener("mousemove", (e) => {
      for (var idx5 in all5) {
        var el5 = all5[idx5];
        if (el5.addEventListener) {
          el5.addEventListener('click', stopFunc5, true);
        }
      }
    });
  });

  slideList5.addEventListener("mouseup", (e) => {
    console.log("mouseup", e.pageX);
    endPoint5 = e.pageX;
    if (startPoint5 < endPoint5) {
      console.log("prev move");
      prevMove5();
    } else if (startPoint5 > endPoint5) {
      console.log("next move");
      nextMove5();
    }

    slideList5.addEventListener("mousemove", (e) => {
      for (var idx5 in all5) {
        var el5 = all5[idx5];
        if (el5.addEventListener) {
          el5.removeEventListener('click', stopFunc5, true);
        }
      }
    });
  });

  slideList5.addEventListener("touchstart", (e) => {
    stopSlide5();
    console.log("touchstart", e.touches[0].pageX);
    startPoint5 = e.touches[0].pageX;
  });

  slideList5.addEventListener("touchend", (e) => {
    console.log("touchend", e.changedTouches[0].pageX);
    endPoint5 = e.changedTouches[0].pageX;

    var diff5 = startPoint5 - endPoint5;
    console.log("diff5", diff5);
    if (Math.abs(diff5) < 6) {} 
    else if (startPoint5 < endPoint5) {
      console.log("prev move");
      prevMove5();
    } else if (startPoint5 > endPoint5) {
      console.log("next move");
      nextMove5();
    }

  });


  slideBox5.addEventListener("touchstart", (e) => {
    stopSlide5();
  });
  slideBox5.addEventListener("touchend", (e) => {
    autoSlide5();
  });

  slideBox5.addEventListener("mouseenter", (e) => {
    e.preventDefault();
    stopSlide5();
  });
  slideBox5.addEventListener("mouseleave", (e) => {
    e.preventDefault();
    autoSlide5();
  });

  slideBtnNext5.addEventListener("click", () => {
    nextMove5();
    console.log("nextMove5");
  });
  slideBtnPrev5.addEventListener("click", () => {
    prevMove5();
    console.log("prevMove5");
  });

})();
