/*탭바*/
$(document).ready(function(){
	$('.js-buytab__open1').on('click',function(e){ //구매하기 클릭시 나오는 함수입니다. 
		var winH = $(window).height();
		e.preventDefault();
		$('body').addClass('is-active');
		
		if(ordOptUseYn == "N" && goodsSaleTypeCd != "0004"){
			$(this).closest(".type-buy").next().find(".buytab__wrap").children("ul").attr("class", "buytab__btn-list type-multi type-open");
			$(this).closest(".type-buy").next().find(".buytab__wrap").children("ul").html(el1);
			
		} else {
			$(this).closest(".type-buy").next().find(".buytab__wrap").children("ul").attr("class", "buytab__btn-list type-two2 type-open");
			$(this).closest(".type-buy").next().find(".buytab__wrap").children("ul").html(el3);
		}

		$(this).parents('.tab-bar').find('.type-detail').addClass('is-active');

		//$(this).closest(".type-buy").next().find(".buytab__wrap").children("ul").attr("class", "buytab__btn-list type-multi type-open");
		//$(this).closest(".type-buy").next().find(".buytab__wrap").children("ul").html(el1);
		
		//$(this).parents('.tab-bar').find('.type-detail').addClass('is-active');
//		regularOrderYn = '0001';
//		$("#ordTypeCd").val(regularOrderYn);
//		$('.black').show();
	});
	$('.js-buytab__open2').on('click',function(e){  //
		var winH = $(window).height();
		e.preventDefault();
		$('body').addClass('is-active');
		
		
		$(this).closest(".type-buy").next().find(".buytab__wrap").children("ul").attr("class", "buytab__btn-list type-two2 type-open");
		$(this).closest(".type-buy").next().find(".buytab__wrap").children("ul").html(el2);
		$(this).parents('.tab-bar').find('.type-detail').addClass('is-active')
		$('.black').show();
	});
	
	
	$('.js-buytab__close').on('click',function(e){
		e.preventDefault();
		$('body').removeClass('is-active');
		$(this).parents('.tab-bar').find('.type-detail').removeClass('is-active')
		$('.black').hide();
	});
	
	$(".select3__header").click(function(){
		var str = $(this).parent().attr('class');
		var selectTitle = $(this).find('p').attr('data-value');
		$(this).find('p').text(selectTitle);
		if(str == 'select3'){
			$(this).parents('.buytab__wrap').addClass('is-active')
			$(this).parent().toggleClass("showMenu").siblings('.select3').hide();
			$('.buytab__option').hide();
		}else{
			$(this).parents('.buytab__wrap').removeClass('is-active')
			$(this).parent().toggleClass("showMenu").siblings('.select3').show();
			$('.buytab__option').show();
		}
	});
	$(".select3__menu > li").click(function(){
		$(this).parents('.buytab__wrap').removeClass('is-active')
		$(this).parents('.select3').find('.select3__header > p').text($(this).text());
		$(this).parents('.select3').removeClass("showMenu").siblings('.select3').show();
		$('.buytab__option').show();
	});
});