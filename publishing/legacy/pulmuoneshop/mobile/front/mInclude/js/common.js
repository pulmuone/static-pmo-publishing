
$(document).ready(function(){
	$(window).load(function(){
		$(window).trigger('resize');

		var filter = "win16|win32|win64|mac|macintel"; 
		if ( navigator.platform ) {
			if ( filter.indexOf( navigator.platform.toLowerCase() ) < 0 ) {
				$('body').css('font-family' ,'Droid Sans, Apple-Gothic, 애플고딕, Tahoma, sans-serif')
				
			}else {
				$('body').css('font-family' ,'Nanum Barun Gothic, sans-serif')
				}
		}

		$('.sale__list').slick({
			centerMode: true,
			centerPadding: '120px',
			slidesToShow: 1,
			prevArrow: '#main__btn-prev',
			nextArrow: '#main__btn-next',
			responsive: [
		    {
		      breakpoint: 460,
		      settings: {
		        centerMode: true,
		        centerPadding: '90px',
		        slidesToShow: 1
		      }
		    },
		    {
		      breakpoint: 360,
		      settings: {
		        centerMode: true,
		        centerPadding: '40px',
		        slidesToShow: 1
		      }
		    }
		  ]
		});
		
		
	})
	
	$('.sale__list').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		$(".main__sale-date").text(saleDt[nextSlide]);
		$('.main__sale-number').html('('+(nextSlide+1)+'/'+todayCnt+')')
	});

	var height = $(window).height();
	//메뉴블랙영역잡기
	$(window).resize(function(){
		var winH = $('body').height();
		var footerH = $('.footer').height()+61;/*푸터픽스드 시킬떄 50추가*/
		// $('.white, .black').height(winH+footerH);
		$('body').css('padding-bottom',footerH)
	});
	//메뉴
	$(function(){
		$('.gnb__1depth-list a').off();
		$('.gnb__1depth-list a').on('click', function(){
			var p = $(this).parents('.gnb__1depth-list');
			console.log(p)
			if($(p).hasClass('on')){
				$(p).removeClass('on').next().hide();
			} else {
				$('.gnb__1depth .on').next().hide();
				$('.gnb__1depth-list').removeClass('on')
				$(p).addClass('on').next().show();
			}
		});
		$('.header__menu-btn').on('click', function(e){
			e.preventDefault();
			$('.gnb').addClass('is-active');
			$('.black').show();
		});
		$('#gnbClose').on('click', function(e){
			e.preventDefault();
			$('.gnb').removeClass('is-active');
			$('.black').hide();
		});
	});



	//tab
	$(function() {
		var tab = $("[data-tab='true']");
		var hash = window.location.hash;
		var tt= [];

		$(tab).find("a").each(function(idx){
			var t = $(tab).find("a").eq(idx).attr("href").split('#')[1];
			tt.push(t);

			if (hash) {
				sele($(tab).find("a[href="+hash+"]"));
				for (var i in tt) {
					$("#"+tt[i]).removeClass("is-active");
				}
				$(hash).addClass("is-active");
			}

			$(this).on("click", function(e){
				if (this.href.match(/#([^ ]*)/g)) {
					e.preventDefault();
					if (!$(this).parent().hasClass("is-active")) window.location.hash = ($(this).attr("href"));
					sele($(this));
					for (var i in tt) {
						$("#"+tt[i]).removeClass("is-active");
					}
					$("#"+t).addClass("is-active");
				}
				$('.tab-v2__item').removeClass('clear')
				$('.tab-v2__item.is-active').prev().addClass('clear');
			});



		})
		if ($(tab).hasClass("tab-v1")) {
			$(tab).find("[class$=list]").on("click", function(){
				//console.log($('body').attr('data-mobile'));
				if ($('body').attr('data-mobile') == 'true'){
					if ($(this).hasClass("js-open-m")) {
						$(this).removeClass("js-open-m");
					} else {
						$(this).addClass("js-open-m");
					}
				}
			});
			$(window).resize(function(){
				if ($('body').attr('data-mobile') == 'false') $(".tab-v1").removeClass("js-open-m");
			});
		}

		function sele(el) {
			$(el).parent().addClass("is-active").siblings().removeClass("is-active");
		}

	});

	//리스트 형태바꾸기
	var cnt = 0
	$('#listbtn').on('click',function(){
		if(cnt == 1){
			$(this).addClass('is-active')
			$('.prod_info').addClass('type-wide').removeClass('box');
			$('.main__content-item').css('width','100%');
			cnt = 0
		}else{
			$(this).removeClass('is-active')
			$('.prod_info').addClass('box').removeClass('type-wide');
			$('.main__content-item').css('width','50%');
			cnt = 1;
		}
	});

	var swiper7 = new Swiper('.story__list', {
		slidesPerView: 2,
		spaceBetween: 10,
		loop: true
	});

	var swiper2 = new Swiper('.prod__slide', {
		pagination: '.swiper-pagination',
		paginationClickable: true,
		nextButton: '#main__btn-next',
		prevButton: '#main__btn-prev',
		loop: true
	});
	var swiper3 = new Swiper('.recipe__list', {
		nextButton: '#recipe__btn-next',
		prevButton: '#recipe__btn-prev',
		loop: true
	});
// 	var swiper4 = new Swiper('.brand__list', {
// 		nextButton: '#brand__btn-next',
// 		prevButton: '#brand__btn-prev',
// 		loop: true
// 	});
	var swiper6 = new Swiper('.event__list', {
		nextButton: '#event__btn-next',
		prevButton: '#event__btn-prev',
		loop: true
	});
	
	var freeNum = $('.cont-slide__item').length
	if(freeNum >= 3){
		var swiper5 = new Swiper('.cont-slide__list',{
			slidesPerView: 2,
			nextButton: '#cont-slide__btn-next',
			prevButton: '#cont-slide__btn-prev',
			buttonDisabledClass : 'is-hidden'
		});
		$('.cont-slide__slide-btn').show();
	}

	$('.prod_set-btn').on('click',function(e){
		 var lineH = $(this).parents('.detail__content-set')
		e.preventDefault();
		lineH.toggleClass('is-active');
	})

	$('.js-set').on('click',function(e){
		var cItem = $(this).parents('.content-set__item')
		e.preventDefault();
		$('.content-set__list').prepend(cItem);
		$('.detail__content-set').removeClass('is-active')
	})

	//탑배너
	$('.js-top-close').on('click',function(e){
		e.preventDefault();
		$(this).parent().addClass('is-hidden');
	});
	//스크롤 탑
	$(window).scroll(function(){
		if($(this).scrollTop()>0){
			$('.top__btn').fadeIn();
			//$('.footer-fix').addClass('is-active')
		}else{
			$('.top__btn').fadeOut();
			//$('.footer-fix').removeClass('is-active')
		}
	});

	$('#topBtn').click(function(){
		$('html,body').animate({'scrollTop':'0'},300)
	});

	var popY = '';
	$('.js-popup-open2, .js-popup-open3, .js-popup-open4, .js-popup-open5, .js-popup-open6, .js-popup-open7, .js-popup-open13, .js-popup-open14').on('click',function(e){
		popY = $(window).scrollTop();
		bodyP2 = $('body').css('padding-bottom').replace(/[^-\d\.]/g, '');
		$('html, body').addClass("is-active");
		$('.body__wrap').css({"position":"relative","top":-popY});
		$('.footer').css({"bottom":-bodyP2});
	});
	$('.js-popup-open').on('click',function(e){
		e.preventDefault();
		$('.popup, .black').show();
	});
	$('.js-popup-close').on('click',function(e){
		e.preventDefault();
		$('html, body').removeClass("is-active");
		$(this).parents('.tab-bar').find('.type-detail').removeClass('is-active')
		$('.popup, .black').hide();
		$('.body__wrap').css("position","static")
		$('.footer').css('bottom','0px')
	});
	$('.js-popup-open1').on('click',function(e){
		e.preventDefault();
		$('.popup.type-tooltip, .black').show();
	});
	$('.js-popup-open3').on('click',function(e){
		e.preventDefault();
		$('.popup.type-calendar, .black').show();
	});
	$('.js-popup-open4').on('click',function(e){
		e.preventDefault();
		$('.popup.type-custom, .black').show();
	});
	$('.js-popup-open5').on('click',function(e){
		e.preventDefault();
		$('.popup.type-sns, .black').show();
	});
	$('.js-popup-open6').on('click',function(e){
		e.preventDefault();
		$('.popup.type-event, .black').show();
	});
	$('.js-popup-open7').on('click',function(e){
		e.preventDefault();
		$('.popup.type-login, .black').show();
	});
	$('.js-popup-open13').on('click',function(e){
		e.preventDefault();
		var id = $(this).attr("popupId");
		$("#"+id).show();
		$('.black').show();
	});
	$('.js-popup-open14').on('click',function(e){
		e.preventDefault();
		$('.popup.type-message2, .black').show();
	});
	$('.payment__btn').on('click',function(e){
		e.preventDefault();
		$(this).parents('.payment__item').toggleClass('on');

	});

	var cnt2 = 1
	$('.js-order').on('click',function(e){
		e.preventDefault();

		if(cnt2 == 1){
			$(this).addClass('is-active');
			$(this).parents('.order__header').next().addClass('is-active');
			cnt2 = 0;
		}else{
			$(this).removeClass('is-active');
			$(this).parents('.order__header').next().removeClass('is-active');
			cnt2 = 1;
		}
	});

	$('.js-delivery').on('click',function(e){
		e.preventDefault();
		$(this).addClass('is-active').siblings().removeClass('is-active')
		var menuName = $(this).attr('data-name')
		if(menuName == 'default'){
			$(this).parents('div').next().addClass('is-active');
			$(this).parents('div').next().next().removeClass('is-active');
		} else if(menuName == 'direct') {
			$(this).parents('div').next().removeClass('is-active');
			$(this).parents('div').next().next().addClass('is-active');
		}
	});


	$('.js-popup-delivery').on('click',function(e){
		e.preventDefault();
		$(this).addClass('is-active').siblings().removeClass('is-active')
		var menuName2 = $(this).attr('data-name')
		if(menuName2 == 'basics'){
			$(this).parents('div').next().find('div:first').addClass('is-active');
			$(this).parents('div').next().find('div:last').removeClass('is-active');
		}else if(menuName2 == 'recently'){
			$(this).parents('div').next().find('div:first').removeClass('is-active');
			$(this).parents('div').next().find('div:last').addClass('is-active');
		}
	});

	$('.order__means-link').on('click',function(e){
		e.preventDefault();
		$('.order__means-link').removeClass('is-active');
		$(this).addClass('is-active');
		var menuName3 = $(this).attr('data-name')
		if(menuName3 == 'case1'){
			$(this).parents('.order__body').find('.js-case1').addClass('is-active').siblings().removeClass('is-active');
		}else if(menuName3 == 'case2'){
			$(this).parents('.order__body').find('.js-case2').addClass('is-active').siblings().removeClass('is-active');
		}else if(menuName3 == 'case3'){
			$(this).parents('.order__body').find('.js-case3').addClass('is-active').siblings().removeClass('is-active');
			$("#dim-vaccount_notify").show();
		}
	});


	$('.js-popup-delivery').on('click',function(e){
		e.preventDefault();
		$(this).addClass('is-active').siblings().removeClass('is-active')
		var menuName4 = $(this).attr('data-name')
		$('.popup_tab-content').removeClass('is-active');
		if(menuName4 == 'email'){
			$('.popup_tab-content.js-email').addClass('is-active');
		} else if(menuName4 == 'phone') {
			$('.popup_tab-content.js-phone').addClass('is-active');
		}
	});
	$(function(){
		prodPost(),
		uiSimpleToggle()
	});

	var prodPost = function() {
		var t = 300,
			i = $(".prod_post"),
			e = i.find(".post_title a, .post_title button");
		$answer = i.find(".post_body"), e.on("click", function(i) {
			$(this).parents("li").is(".active") ? ($answer.stop(!0, !0).slideUp(t), $answer.parent("li").removeClass("active")) : ($(this).parent().next($answer).stop(!0, !0).slideDown(t).parent("li").siblings().find($answer).stop(!0, !0).slideUp(t), $(this).parents("li").addClass("active").siblings().removeClass("active")), i.preventDefault()
		});
	},

	uiSimpleToggle = function() {
		var t = $("ul.uiToggle"),
			i = t.find("button.postbutton, a.postbutton");
		i.on("click", function() {
			$(this).parent("li").is(".active") ? $(this).parent("li").removeClass("active") : $(this).parent("li").addClass("active").siblings().removeClass("active")
		});
	};

	$('.my_menu_set li .toggle').click(function(){
		if($(this).hasClass('on')){
     		$(this).removeClass('on');
		}
		else {
			$(this).addClass('on');
		}
	});

	$('.star_control .star1').click(function(){
		if($('.star_control').hasClass('star1')){
     		$('.star_control').removeClass('star1');
		}
		if($('.star_control').hasClass('star2')){
     		$('.star_control').removeClass('star2');
		}
		if($('.star_control').hasClass('star3')){
     		$('.star_control').removeClass('star3');
		}
		if($('.star_control').hasClass('star4')){
     		$('.star_control').removeClass('star4');
		}
		if($('.star_control').hasClass('star5')){
     		$('.star_control').removeClass('star5');
		}
		else {
			$('.star_control').addClass('star1');
		}
	});
	$('.star_control .star2').click(function(){
		if($('.star_control').hasClass('star1')){
     		$('.star_control').removeClass('star1');
		}
		if($('.star_control').hasClass('star2')){
     		$('.star_control').removeClass('star2');
		}
		if($('.star_control').hasClass('star3')){
     		$('.star_control').removeClass('star3');
		}
		if($('.star_control').hasClass('star4')){
     		$('.star_control').removeClass('star4');
		}
		if($('.star_control').hasClass('star5')){
     		$('.star_control').removeClass('star5');
		}
		else {
			$('.star_control').addClass('star2');
		}
	});
	$('.star_control .star3').click(function(){
		if($('.star_control').hasClass('star1')){
     		$('.star_control').removeClass('star1');
		}
		if($('.star_control').hasClass('star2')){
     		$('.star_control').removeClass('star2');
		}
		if($('.star_control').hasClass('star3')){
     		$('.star_control').removeClass('star3');
		}
		if($('.star_control').hasClass('star4')){
     		$('.star_control').removeClass('star4');
		}
		if($('.star_control').hasClass('star5')){
     		$('.star_control').removeClass('star5');
		}
		else {
			$('.star_control').addClass('star3');
		}
	});
	$('.star_control .star4').click(function(){
		if($('.star_control').hasClass('star1')){
     		$('.star_control').removeClass('star1');
		}
		if($('.star_control').hasClass('star2')){
     		$('.star_control').removeClass('star2');
		}
		if($('.star_control').hasClass('star3')){
     		$('.star_control').removeClass('star3');
		}
		if($('.star_control').hasClass('star4')){
     		$('.star_control').removeClass('star4');
		}
		if($('.star_control').hasClass('star5')){
     		$('.star_control').removeClass('star5');
		}
		else {
			$('.star_control').addClass('star4');
		}
	});
	$('.star_control .star5').click(function(){
		if($('.star_control').hasClass('star1')){
     		$('.star_control').removeClass('star1');
		}
		if($('.star_control').hasClass('star2')){
     		$('.star_control').removeClass('star2');
		}
		if($('.star_control').hasClass('star3')){
     		$('.star_control').removeClass('star3');
		}
		if($('.star_control').hasClass('star4')){
     		$('.star_control').removeClass('star4');
		}
		if($('.star_control').hasClass('star5')){
     		$('.star_control').removeClass('star5');
		}
		else {
			$('.star_control').addClass('star5');
		}
	});

	$(function(){
		$(".accordian5_item_header").unbind();
		$('.accordian5_item').addClass('on').find('.accordian5_item_body').css('display','block');
		$('.js-accordian1_all .accordian5_item').addClass('on').find('.accordian5_item_body').css('display','block');
		$(".accordian5_item_header").on("click keypress",function(e){
			var $target = $(this).parent("li");
			if ((e.keyCode == 13)||(e.type == 'click'))$(this).parent("li").toggleClass("on");
			if ( $target.is('.on')){
				$(this).next().slideDown('500');
			} else {
				$(this).next().slideUp('500');
			}
		});
	});

	$('zoom__btn').off();
	var posY;
	$('.zoom__btn').on('click',function(e){
		e.preventDefault();
		posY = $(window).scrollTop();
		bodyP = $('body').css('padding-bottom').replace(/[^-\d\.]/g, '');
		$('html, body').addClass("is-active");
		$('.body__wrap').css({"position":"relative","top":-posY});
		$('.footer').css({"bottom":-bodyP});
		$('.zoom').show();
		var swiper9 = new Swiper('.zoom__slide', {
			zoom: true,
			pagination: '.swiper-pagination',
			pagination: '.swiper-pagination',
			paginationType: 'fraction'
		});
	});
	$('.js-zoom-close').on('click',function(e){
		e.preventDefault();
		$('html, body').removeClass("is-active");
		$(this).parents('.tab-bar').find('.type-detail').removeClass('is-active');
		$('.body__wrap').css("position","static");
		posY = $(window).scrollTop(posY);
		$('.footer').css('bottom','0px');
		$('.zoom').hide();
	});


});

$(function() {
	$('.tab-v5__link').on('click',function(e){
		e.preventDefault();
		$(this).addClass('is-active').siblings().removeClass('is-active')
	});
	
	
});




$(document).ready(function(){
	bodyresize(); 
});

$(window).bind("resize", function() {
	bodyresize();
});

var deBodyWidth = 0;
var deBodyHeight = 0;

function bodyresize() {
	var defaultwidth = 320;
	var bodywidth = $("body").width();
	if( deBodyWidth == 0 || deBodyWidth != bodywidth) {
		deBodyWidth = bodywidth;
		var bodyratio = bodywidth / defaultwidth;
		var bodyfontsize = 10 * bodyratio;
		$("body").css("font-size", bodyfontsize);
	}

}