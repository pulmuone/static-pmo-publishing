$(function() {
	// 조건검색(카테고리)
	$(".accordian5_item_header").unbind();
	$('.accordian5_item').addClass('on').find('.accordian5_item_body').css('display','block');
	$('.js-accordian1_all .accordian5_item').addClass('on').find('.accordian5_item_body').css('display','block');
	$(".accordian5_item_header").on("click keypress",function(e) {
		var $target = $(this).parent("li");
		
		if((e.keyCode == 13) || (e.type == 'click')) {
			$(this).parent("li").toggleClass("on");
		}
		
		if($target.is('.on')) {
			$(this).next().slideDown('500');
		}else{
			$(this).next().slideUp('500');
		}
	});
	
	// ###################### 통합검색 ######################
	// 자동완성검색 keyup 이벤트
	$("#headerQuery").keyup(function(event) {
		if(event.keyCode != 13) {
			insideSearch(this.value);
		}else{
			headerSearch(this.value);
		}
	});
	// #####################################################
});

// 검색창 닫기
$('.header__search-close').on('click',function(e){
	e.preventDefault();
	$('.search-layer.type-popular').hide();
});

// 검색창 포커스
$(document).ready(function(){
	// 자동완성 검색 포커스
	$('.js-search-input').on('focus',function(){
		$('.search-layer.type-popular, .header__search-close').show();
	});
	$('.js-search-input').on('blur',function(){
		setTimeout(function(){$('.search-layer.type-popular, .header__search-close').hide();},200);
	});
	
	// 카테고리 쪽 스크롤 바
	var posY = "";
	
	$('.js-search-open').on('click',function(e){
		e.preventDefault();
		posY = $(window).scrollTop();
		var bodyP = $('body').css('padding-bottom').replace(/[^-\d\.]/g, '');
		$('html, body, .search-bar').addClass("is-active");
		$('.black').show();
		$('.body__wrap').css({"position":"relative","top":-posY});
		$('.footer').css({"bottom":-bodyP});
	});
	$('.js-search-close').on('click',function(e){
		e.preventDefault();
		$('html, body, .search-bar').removeClass('is-active')
		$('.black').hide();
		$('.body__wrap').css("position","static")
		posY = $(window).scrollTop(posY);
		$('.footer').css('bottom','0px')
	});
	$(".search-bar__list").mCustomScrollbar({
		theme:"minimal-dark", 
		advanced:{
			updateOnContentResize: true
		}
	});
	//listView 로딩 처리
	var listview = $("#listView").val();
	//alert(listview);
	if(listview == "0"){
		$(this).addClass('is-active')
		$('.prod_info').addClass('type-wide');
		$('.main__content-item').css('width','100%');		
		//alert("1111");
	}else{
		$(this).removeClass('is-active')
		$('.prod_info').removeClass('type-wide');
		$('.main__content-item').css('width','50%');
		//alert("2일떄");
	}
	
});

/** ############################################################ 통합 검색 ajax : dataType="JSON" ############################################################ */
// 자동완성검색 ajax(JSON) 함수 구현
function insideSearch(query) {
	var str		 = "";
	var target	 = "common";
	var query	 = query;
	var convert	 = "fw";
	var datatype = "json";
	
	$.ajax({
		url			: "/search/mSearchAuto.do",	// JSON URL 요청
		type		: "POST",					// 전송방식[GET, POST]
		cache		: false,					// 캐시사용
		async		: true,						// 비동기 true, 동기 false
		dataType	: "json",					// ajax 데이터 타입(xml, text, json)
		data : {
			"target"	: target,
			"query"		: query,
			"convert"	: convert,
			"datatype"	: datatype,
			"charset"	: "UTF-8"
		},
		success : function(data) {
			if(data.result.length <= 0) {
				totalFwCount = 0;
				totalRwCount = 0;
			}
			
			$.each(data.result, function(i, result) {
				var totalCount = parseInt(result.totalcount);
				
				if(i == 0) {
					totalFwCount = totalCount;
				}else{
					totalRwCount = totalCount;
				}
				
				if(totalCount > 0) {
					// 정방향, 역방향 구분선
					if(i > 0 && totalFwCount > 0 && totalRwCount > 0) {
						str += "<li style=\"border-top:1px solid #f3f3f3;\"></li>";
					}
					
					// 자동완성 리스트 설정
					$.each(result.items, function(num, item) {
						str += "<li class='search-layer__item'>";
						str += "	<a href='/search/mSearchMain.do?query="+item.keyword+"' class='search-layer__link'>";
						str += 			item.hkeyword;
						str += "	</a>";
						str += "</li>";
					});
				}
			});
			
			// 해당 선택자(ID)에 맞는 html에 변수 str값을 세팅해서 최종 페이지에 뿌려준다.
			if((totalFwCount + totalRwCount) == 0) {
				$("#schUlGoods").html("<li class='search-layer__item' style=\"text-align: center;\"><a href='#' class='search-layer__link'>'"+query+"'(으)로 검색 된 결과가 없습니다.</a></li>");
			}else{
				$("#schUlGoods").html(str);
			}
		},
		error : function(request, status, error) {
//			alert("status:" + status + ", error:" + error + "</br>");
		}
	});
}

// 더보기 ajax
function searchPlus(queryPlus, startCnt, vrCnt, gSort, cateNo, brandNo, deliStr,isCompanyFlg_str) {
	
	var str				= "";
	var query			= queryPlus;
	var startCount		= parseInt(startCnt);
	var viewResultCount	= parseInt(vrCnt);
	var collection		= "GOODS";
	var sort			= gSort;
	var categoryCheck	= cateNo;
	var brandCheck		= brandNo;
	var deliveryCheck	= deliStr;
	var isCompanyPlusFlg = isCompanyFlg_str;
	
	$.ajax({
		url		 : "/search/mSearchJson.do",
		type	 : "POST",
		cache	 : false,
		async	 : true,
		dataType : "json",
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		data : {
			"query"			  : query,
			"collection"	  : collection,
			"startCount"	  : startCount,
			"viewResultCount" : viewResultCount,
			"sort"			  : sort,
			"category_check"  : categoryCheck,
			"brand_check"	  : brandCheck,
			"delivery_check"  : deliveryCheck,
			"charset"		  : "UTF-8"
		},
		success	 : function(data) {
			var list = data.SearchQueryResult.Collection[0].DocumentSet.Document;	// 객체 호출(검색결과 배열)
			var isCompany = $("#isCompanyFlg").val();			
			
			$.each(list, function(index, value) {
				// 데이터 필드 변수 세팅
				var item = value.Field; 
				
				// 상품 쪽 파라미터 값 변수 세팅
				var GOODS_NO		= "";									// 모바일 상품번호
				var GOODS_NM		= "";									// 모바일 상품명
				var CATE_CD			= ""									// 카테고리 번호
				var DISP_GOODS_NM	= "";									// 모바일 상품명
				var M_IMG_FILE_NM	= "";									// 모바일 이미지
				var SALE_PRICE		= "";									// 할인 가격
				var NORMAL_PRICE	= "";									// 정상 가격
				var REAL_MOB_RATE 	= "";									// 모바일_할인율
				var REAL_MEM_RATE 	= "";									// 모바일_직원할인율
				var REAL_MOB_PRICE  = "";									// 모바일_할인가
				var REAL_MEM_PRICE  = "";									// 모바일_임직원할인가
				var ICON_GIFT_YN  	= "";									// 증정 아이콘
				var DELV_ICON_DAILY_YN = "";								// 일일배송아이콘
				var DELV_ICON_FREE_YN = "";									// 무료배송아이콘
				var DELV_ICON_DIRECT_YN = "";								// 생산직송아이콘
				var DELV_ICON_FAST_YN = "";									// 새벽배송아이콘
				var GOODS_STAT_CD = "";										// 품절여부
				var Icon_Pack_Yn = "";										// 묶음아이콘여부
				var PACK_CONT_DESC = "";									// 묶음내용표시
				var ORD_OPT_USE_YN = "";									// 옵션 사용 여부
				var GOODS_SALE_TYPE_CD = "";								// 상품판매타입
				
				$.each(item, function(index, value) {
					if(index == "GOODS_NO") {
						GOODS_NO = value;
					} else if(index == "CATE_CD") {
						CATE_CD = value;
					} else if(index == "GOODS_NM") {
						GOODS_NM = replaceAll(replaceAll(value,"<!HS>","<font color=#f96c2b style='font-weight: bold;'>"),"<!HE>","</font>");
					} else if(index == "DISP_GOODS_NM") {
						DISP_GOODS_NM = replaceAll(replaceAll(value,"<!HS>","<font color=#f96c2b style='font-weight: bold;'>"),"<!HE>","</font>");
					} else if(index == "M_IMG_FILE_NM") {
						M_IMG_FILE_NM = value;
					} else if (index == "SALE_PRICE") {
						SALE_PRICE = comma(value);
					} else if (index == "NORMAL_PRICE") {
						NORMAL_PRICE = comma(value);
					} else if (index == "REAL_MOB_RATE") {
						REAL_MOB_RATE = comma(value);
					} else if (index == "REAL_MEM_RATE") {
						REAL_MEM_RATE = comma(value);
					} else if (index == "REAL_MOB_PRICE") {
						REAL_MOB_PRICE = comma(value);
					} else if (index == "REAL_MEM_PRICE") {
						REAL_MEM_PRICE = comma(value);
					} else if (index == "ICON_GIFT_YN") {
						ICON_GIFT_YN = comma(value);
					} else if (index == "DELV_ICON_DAILY_YN") {
						DELV_ICON_DAILY_YN = comma(value);
					} else if (index == "DELV_ICON_FREE_YN") {
						DELV_ICON_FREE_YN = comma(value);
					} else if (index == "DELV_ICON_DIRECT_YN") {
						DELV_ICON_DIRECT_YN = comma(value);
					} else if (index == "DELV_ICON_FAST_YN") {
						DELV_ICON_FAST_YN = comma(value);
					} else if (index == "GOODS_STAT_CD") {
						GOODS_STAT_CD = comma(value);
					} else if (index == "Icon_Pack_Yn") {
						Icon_Pack_Yn = comma(value);
					} else if (index == "PACK_CONT_DESC") {
						PACK_CONT_DESC = comma(value);
					} else if (index == "ORD_OPT_USE_YN") {
						ORD_OPT_USE_YN = comma(value);
					} else if (index == "GOODS_SALE_TYPE_CD") {
						GOODS_SALE_TYPE_CD = value;
					}
								
				});
				
				//임직원 / 비회원 판별
				var display_price = "";
				var display_rate = "";
				
				if(isCompanyPlusFlg == "true"){										
					display_price = REAL_MEM_PRICE;
					display_rate = 	REAL_MEM_RATE;
				}

				if(isCompanyPlusFlg == "false"){
					display_price = REAL_MOB_PRICE;
					display_rate = 	REAL_MOB_RATE;
				}			
				
				var img_url = GOODS_IMG_URL + "/" + GOODS_NO + "/" + M_IMG_FILE_NM;				
				// 상품 상세페이지 이동
				//var dispView = "javascript:goGoodsDetail('"+CATE_CD+","+GOODS_NO+"')";
				//20170919 검색후 더보기 클릭시 상품상세페이지 에러 현상 수정 : 박미영
				var dispView = "javascript:goGoodsDetail('"+CATE_CD+"','"+GOODS_NO+"')";
				
				//listview가 0 일시 가로 타입
				if($('#viewType').hasClass('prod_info type-wide')){
					str += '<li class="main__content-item" style="width: 100%;">';
					str += '	<div class="prod_info type-wide">';
					str += '		<a href="'+dispView+'" class="prod_img"><img src="'+img_url+'" alt="" /></a>';
					str += '		<div class="prod_detail">';
					str += '			<a href="'+dispView+'">';
					if(GOODS_STAT_CD == "0004"){
						str += "<div class='soldout'><p class='soldout__wrap'>품절</p></div>";					
					}else if(GOODS_STAT_CD == "0003"){
						str += "<div class='soldout'><p class='soldout__wrap'>판매중지</p></div>";					
					}				
					str += '				<span class="prod_detail_top">';
					str += '								<span class="prod_tit_wide">'+GOODS_NM+'</span>';
					str += '					<span class="prod_price_wide">';
					str += '						<strong class="prod_price_now">';
					str += '							<span>'+display_price+'</span>원';
					str += '						</strong>';
					if(display_price != NORMAL_PRICE){					
						str += '						<span class="prod_price_prev"><span>'+NORMAL_PRICE+'</span>원</span>';
					}
					str += '					</span>';
					str += '				</span>';
					str += "				<div class='prod_btn-area-m'>";
					if(display_rate > 0){
						str += "					<div class='prod_btn prod_sale'><div class='prod_btn-wrap'>"+display_rate+"<span>%</span></div></div>";					
					}
					if(ICON_GIFT_YN == "Y"){
						str += "					<div class='prod_btn prod_present'><div class='prod_btn-wrap'>증정</div></div>";					
					}
					if(Icon_Pack_Yn == "Y"){
						str += "					<div class='prod_btn prod_present'><div class='prod_btn-wrap'>"+ PACK_CONT_DESC +"</div></div>";					
					}
					
					if(DELV_ICON_FREE_YN == "Y"){
						str += "					<div class='prod_btn prod_delivery'><div class='prod_btn-wrap'>무료<br>배송</div></div>";
					}
					if(DELV_ICON_DAILY_YN == "Y"){
						str += "					<div class='prod_btn prod_delivery2'><div class='prod_btn-wrap'>일일<br>배달</div></div>";
					}
					if(DELV_ICON_DIRECT_YN == "Y"){
						str += "					<div class='prod_btn prod_delivery2'><div class='prod_btn-wrap'>생산<br>직송</div></div>";
					}
					if(DELV_ICON_FAST_YN == "Y"){
						str += "					<div class='prod_btn prod_delivery3'><div class='prod_btn-wrap'>바른<br>배송</div></div>";
					}
					str += "				</div>";
					str += '			</a>';
					if(GOODS_STAT_CD == "0004"){
						str += '<a href="#none" onClick="javascript:restockAlarm('+"'"+img_url+"','"+replaceTag(item.GOODS_NM)+"',"+item.GOODS_NO+');" class="prod_btn_cart">';
						str +='    <img src="/front/mInclude/img/btn_stock_info.png" alt="재입고 알림" />';
						str += '</a>';
					}else{
						if(ORD_OPT_USE_YN != "Y" && GOODS_SALE_TYPE_CD != "0004"){
							//alert("xx");
							str += '			<a href="javascript:addCart('+ GOODS_NO +', cnt, 1, true)" class="prod_btn_cart"><img src="/front/mInclude/img/btn_prod_cart.png" alt="장바구니 담기"/></a>';							
						}
					}				
					
					str += '		</div>';
					str += '	</div>';
					str += '</li>';
					
				}else {
					//listview가 1일시, 세로타입
					
					str += '<li class="main__content-item" style="width: 50%;">';
					str += '	<div class="prod_info box">';
					str += '		<a href="'+dispView+'" class="prod_img"><img src="'+img_url+'" alt="" /></a>';
					str += '		<div class="prod_detail">';
					str += '			<a href="'+dispView+'">';
					if(GOODS_STAT_CD == "0004"){
						str += "<div class='soldout'><p class='soldout__wrap'>품절</p></div>";					
					}else if(GOODS_STAT_CD == "0003"){
						str += "<div class='soldout'><p class='soldout__wrap'>판매중지</p></div>";					
					}				
					str += '				<span class="prod_detail_top">';
					str += '								<span class="prod_tit_wide">'+GOODS_NM+'</span>';
					str += '					<span class="prod_price_wide">';
					str += '						<strong class="prod_price_now">';
					str += '							<span>'+display_price+'</span>원';
					str += '						</strong>';
					if(display_price != NORMAL_PRICE){					
						str += '						<span class="prod_price_prev"><span>'+NORMAL_PRICE+'</span>원</span>';
					}
					str += '					</span>';
					str += '				</span>';
					str += "				<div class='prod_btn-area'>";
					if(display_rate > 0){
						str += "					<div class='prod_btn prod_sale'><div class='prod_btn-wrap'>"+display_rate+"<span>%</span></div></div>";					
					}
					if(ICON_GIFT_YN == "Y"){
						str += "					<div class='prod_btn prod_present'><div class='prod_btn-wrap'>증정</div></div>";					
					}
					if(DELV_ICON_FREE_YN == "Y"){
						str += "					<div class='prod_btn prod_delivery'><div class='prod_btn-wrap'>무료<br>배송</div></div>";
					}
					if(DELV_ICON_DAILY_YN == "Y"){
						
						str += "					<div class='prod_btn prod_delivery2'><div class='prod_btn-wrap'>일일<br>배달</div></div>";
					}
					if(DELV_ICON_DIRECT_YN == "Y"){
						str += "					<div class='prod_btn prod_delivery2'><div class='prod_btn-wrap'>생산<br>직송</div></div>";
					}
					if(DELV_ICON_FAST_YN == "Y"){
						str += "					<div class='prod_btn prod_delivery3'><div class='prod_btn-wrap'>바른<br>배송</div></div>";
					}
					str += "				</div>";
					str += '			</a>';
					
					if(GOODS_STAT_CD == "0004"){
						str += '<a href="#none" onClick="javascript:restockAlarm('+"'"+img_url+"','"+replaceTag(item.GOODS_NM)+"',"+item.GOODS_NO+');" class="prod_btn_cart">';
						str +='    <img src="/front/mInclude/img/btn_stock_info.png" alt="재입고 알림" />';
						str += '</a>';
					}else{
						if(ORD_OPT_USE_YN == "N" && GOODS_SALE_TYPE_CD != "0004"){
							//alert("xx");
							str += '			<a href="javascript:addCart('+ GOODS_NO +', cnt, 1, true)" class="prod_btn_cart"><img src="/front/mInclude/img/btn_prod_cart.png" alt="장바구니 담기"/></a>';							
						}
					}
					
					str += '		</div>';
					str += '	</div>';
					str += '</li>';
				}
				
				
//				textCount++;
			});
			
			if(totalCount > 0) {
				$("#mGoodsRL").append(str);
			}
		},
		error : function() {
			//alert("더보기 에러");
		}
	});
}

// 연관검색어
function getRecommend() {
	var str			= "";
	var query		= $("#headerQuery").val().trim();
	var target		= "recommend";
	var label		= "_ALL_";
	var datatype	= "json";
	
	//키워드가 공백일 시, default 연관검색어 노출
	  if(query == ""){
		  query = "recommend";
	  }
	
	$.ajax({
		type : "POST",
		url : "/search/mSearchRecom.do",
		dataType : datatype,
		data : {
			"target"	: target,
			"query"		: query,
			"label"		: label,
			"charset"	: "UTF-8",
			"datatype"	: datatype
		},
		success : function(data) {
			var word = data.Data.Word;
			
			if(word != "undefined" && word != null && word != "") {
				$(".section-sub__box").show();
				
				str += "<p class='search__tit'>연관검색어</p>";
				str += "<ul class='search__list'>";
				
				for(var i=0; i<word.length; i++) {
		 			str += "<li class='search__item'>";
					str += "	<a href='/search/mSearchMain.do?query="+word[i]+"' class='search__link'>"+word[i]+"</a>";
					str += "</li>";
				}
				
				str += "</ul>";
				
				$(".section-sub__box").html(str);
			}
		},
		error : function(request, status, error) {
			//alert("status:" + status + ", error:" + error+"</br>");
		}
	});
}

// 인기검색어
function getPopkeyword() {
	var str			= "";
	var target		= "popword";
	var range		= "W";
	var collection	= "_ALL_";
	var datatype	= "text";
	
	$.ajax({
		type : "POST",
		url : "/search/mSearchPop.do",
		dataType : datatype,
		data : {
			"target"	 : target,
			"range"		 : range,
			"collection" : collection,
			"datatype"	 : datatype
		},
		success: function(text) {
			text = trim(text);
			var xml = $.parseXML(text);
			
			$(xml).find("Query").each(function() {
				str += "<li class='search__item'>";
				str += "	<a href='/search/mSearchMain.do?query="+$(this).text()+"' class='search__link'>"+$(this).text()+"</a>";
				str += "</li>";
			});
			
			$(".search__list").html(str);
		},
		error : function(request, status, error) {
			//alert("status:" + status + ", error:" + error+"</br>");
		}
	});
}

// 검색 결과 없을시 - 베스트 상품 4개 출력
function getNoData() {
	var str				= "";
	var query			= "";
	var collection		= "ALL";
	var startCount		= 0;
	var viewResultCount	= 4;
	var sort			= "SALE_CNT/DESC";
	var datatype		= "json";
	
	$.ajax({
		type	 : "POST",
		url		 : "/search/mSearchJson.do",
		cache	 : false,
		async	 : true,
		dataType : datatype,
		data : {
			"query"			  : query,
			"collection"	  : collection,
			"startCount"	  : startCount,
			"viewResultCount" : viewResultCount,
			"sort"			  : sort,
			"charset"		  : "UTF-8",
			"datatype"		  : datatype
		},
		success : function(data) {
			var list = data.SearchQueryResult.Collection[0].DocumentSet.Document;	// 객체 호출(검색결과 배열)
			
			$.each(list, function(index, value) {
				// 데이터 필드 변수 세팅
				var item = value.Field;
				
				// 상품 쪽 파라미터 값 변수 세팅
				var imgPATH			= "http://222.231.17.21/images/goods";	// 기본 이미지 URL Path
				var imgURL			= "";									// 기본 이미지 URL
				var noImg_1			= imgPATH + "/0/2010070909025050";		// 없는 이미지 URL 1
				var noImg_2			= "this.src='http://121.254.255.44/front/mInclude/img/logo.png';";	// 없는 이미지 URL 2
				var GOODS_NO		= "";									// 모바일 상품번호
				var GOODS_NM		= "";									// 모바일 상품명
				var DISP_GOODS_NM	= "";									// 모바일 상품명
				var M_IMG_FILE_NM	= "";									// 모바일 이미지
				var SALE_PRICE		= "";									// 할인 가격
				var NORMAL_PRICE	= "";									// 정상 가격
				
				$.each(item, function(index, value) {
					if(index == "GOODS_NO") {
						GOODS_NO = value;
					} else if(index == "GOODS_NM") {
						GOODS_NM = replaceAll(replaceAll(value,"<!HS>","<font color=#f96c2b style='font-weight: bold;'>"),"<!HE>","</font>");
					} else if(index == "DISP_GOODS_NM") {
						DISP_GOODS_NM = replaceAll(replaceAll(value,"<!HS>","<font color=#f96c2b style='font-weight: bold;'>"),"<!HE>","</font>");
					} else if(index == "M_IMG_FILE_NM") {
						M_IMG_FILE_NM = value;
					} else if (index == "SALE_PRICE") {
						SALE_PRICE = comma(value);
					} else if (index == "NORMAL_PRICE") {
						NORMAL_PRICE = comma(value);
					}
				});
					
				if(GOODS_NO != "" && M_IMG_FILE_NM != "") {
					imgURL = imgPATH + "/" + GOODS_NO + "/" + M_IMG_FILE_NM;
				}else{
					imgURL = noImg_1;
				}
				
				str += '<li class="main__content-item">';
				str += '	<div class="prod_info">';
				str += '		<a href="#" class="prod_img"><img src="'+noImg_1+'" alt="" onerror="'+noImg_2+'"/></a>';
				str += '		<div class="prod_detail">';
				str += '			<a href="#">';
				str += '				<span class="prod_detail_top">';
				str += '					<span class="prod_tit">'+GOODS_NM+'</span>';
				str += '					<span class="prod_price">';
				str += '						<strong class="prod_price_now">';
				str += '							<span>'+comma(SALE_PRICE)+'</span>원';
				str += '						</strong>';
				str += '						<span class="prod_price_prev"><span>'+comma(NORMAL_PRICE)+'</span>원</span>';
				str += '					</span>';
				str += '				</span>';
				str += '				<div class="prod_btn-area">';
				str += '					<div class="prod_btn prod_sale"><div class="prod_btn-wrap">40<span>%</span></div></div>';
				str += '					<div class="prod_btn prod_present"><div class="prod_btn-wrap">증정</div></div>';
				str += '					<div class="prod_btn prod_delivery"><div class="prod_btn-wrap">무료<br>배송</div></div>';
				str += '					<div class="prod_btn prod_delivery"><div class="prod_btn-wrap">일일<br>배송</div></div>';
				str += '				</div>';
				str += '			</a>';
				str += '			<a href="#" class="prod_btn_cart"><img src="http://121.254.255.44/front/mInclude/img/btn_prod_cart.png" alt="장바구니 담기"/></a>';
				str += '		</div>';
				str += '	</div>';
				str += '</li>';
			});
			
			$("#mGoodsNoRL").html(str);
		},
		error : function() {
			//alert("베스트상품 에러");
		}
	});
}

// 통합검색
function headerSearch(query) {

	//ga tracking;
	var keyword = $("#headerQuery").val();
	ga('send', 'event', 'MW_HEAD', 'SRCH', keyword);

	var searchForm = document.search;
	
	searchForm.query.value = query;
	// 상품쪽 sort는 기본값이 있어 따로 선언 필요X
	searchForm.sort.value = "RANK";
	searchForm.startCount.value = 0;
	searchForm.collection.value = "ALL";
	searchForm.listView.value = $("#listView").val();	
	
	searchForm.submit();
}

// 선택검색(카테고리)
function selCateSearch(query) {
	var searchForm = document.search;
	
	searchForm.query.value = query;
	// 상품쪽 sort는 기본값이 있어 따로 선언 필요X
	searchForm.sort.value = "RANK";
	searchForm.startCount.value = 0;
	searchForm.collection.value = "ALL";
	
	searchForm.submit();
}

// 체크박스 선택 해제(범위:전체)
function chkAllClear() {
	$(".checkbox1").prop("checked", false);
}

// 조건검색시(브랜드) checked 유지
function isChecked(ctNo, brNo, delStr) {
	if(ctNo != "" || brNo != "" || delStr != "") {
		// 카테고리
		var cateNum = ctNo.split(",");
		
		for(var i in cateNum) {
			var naming = "#category_check_" + cateNum[i];
			
			if($(naming).is(":checked") == false) {
				$(naming).attr("checked", true);
			}else{
				$(naming).attr("checked", false);
			}
		}
		
		// 브랜드
		var brandNum = brNo.split("|");
		
		for(var i in brandNum) {
			var naming = "#brand_check_" + brandNum[i];
			
			if($(naming).is(":checked") == false) {
				$(naming).attr("checked", true);
			}else{
				$(naming).attr("checked", false);
			}
		}
		
		// 배송
		var deliveryName = delStr.split(",");
		
		for(var j in deliveryName) {
			var naming = "input:checkbox[name='delivery_check']:checkbox[value='"+deliveryName[j]+"']";
			
			if($(naming).is(":checked") == false) {
				$(naming).attr("checked", true);
			}else{
				$(naming).attr("checked", false);
			}
		}
	}else{
		return false;
	}
}

// Sort 정렬 처리
function sorting(val) {
	var searchForm = document.search;
	searchForm.goods_sort.value = val;
	
	searchForm.submit();
}

// Replace All
function replaceAll(str, orgStr, repStr) {
	return str.split(orgStr).join(repStr);
}

// 공백 제거
function trim(str) {
	return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}

// 콤마찍기
function comma(str) {
	str = String(str);
	return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}
/** ############################################################ 통합 검색 End ############################################################ */
//	var str3 = no.replace(/[|\"]/g,"");

//상품 상세 보기
function goGoodsDetail(cateCd,goodsNo){	
	$("form[name=headf]").attr("action","/goods/displayView.do");
	$("input[name=headCateCd]").val(cateCd);
	$("input[name=headGoodsNo]").val(goodsNo);
	location.href = "/goods/displayView.do?headCateCd=" + cateCd+ "&headGoodsNo=" +goodsNo;
}

//레시피 상세보기
function goDetailRecipe(pcbSeq){
	$("#pcbSeq").val(pcbSeq);
	document.search.method = "post";
	document.search.action = "/recipeView.do"
	document.search.submit();
}

//이벤트&기획전 상세보기
function goDetailEvent(type, seq){	
	$("#srchTitle").val(type);
	$("#srchSeq").val(seq);
	document.search.method = "post";
	document.search.action = "/event/eventPlanView.do";
	
	document.search.submit();
}