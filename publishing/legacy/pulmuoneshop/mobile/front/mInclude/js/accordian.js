$(function(){
	$(".accordian1_item_header").unbind();
	$('.accordian1_item:first-child').addClass('on').find('.accordian1_item_body').css('display','block');
	$('.js-accordian1_all .accordian1_item').addClass('on').find('.accordian1_item_body').css('display','block');
	$(".accordian1_item_header").on("click keypress",function(e){
		var $target = $(this).parent("li");
		if ((e.keyCode == 13)||(e.type == 'click'))$(this).parent("li").toggleClass("on").siblings().removeClass("on");
		if ( $target.is('.on')){
			$(this).next().slideDown('500');
			$target.siblings().find('.accordian1_item_body').slideUp('500');
		} else {
			$(this).next().slideUp('500');
		}
	});
});


$(function(){
	$(".accordian2_item_header").unbind();
	$('.accordian2_item:first-child').addClass('on').find('.accordian2_item_body').css('display','block');
	$(".accordian2_item_header").on("click keypress",function(e){
		var $target = $(this).parent("li");
		if ((e.keyCode == 13)||(e.type == 'click'))$(this).parent("li").toggleClass("on").siblings().removeClass("on");
		if ( $target.is('.on')){
			$(this).next().slideDown('500');
			$target.siblings().find('.accordian2_item_body').slideUp('500');
		} else {
			$(this).next().slideUp('500');
		}
	});
});

$(function(){

	$('.accordian3_item:first-child').addClass('on').find('.accordian3_item_body').css('display','block');
	$(".accordian3_item_header_wrap").on("click keypress",function(e){
		var $target = $(this).parents("li");
		if ((e.keyCode == 13)||(e.type == 'click'))$(this).parents("li").addClass("on").siblings().removeClass("on");
		if ( $target.is('.on')){
			$(this).parents('.accordian3_item_header').next().slideDown('500');
			$target.siblings().find('.accordian3_item_body').slideUp('500');
		} else {
			$(this).parents('.accordian3_item_header').next().slideUp('500');
		}
	});
});

$(function(){
	$(".accordian4_item_header").unbind();
	$('.accordian4_item:first-child').addClass('on').find('.accordian4_item_body').css('display','block');
	$(".accordian4_item_header").on("click keypress",function(e){
		var $target = $(this).parent("li");
		if ((e.keyCode == 13)||(e.type == 'click'))$(this).parent("li").toggleClass("on").siblings().removeClass("on");
		if ( $target.is('.on')){
			$(this).next().stop().slideDown('500');
			$target.siblings().find('.accordian4_item_body').slideUp('500');
		} else {
			$(this).next().stop().slideUp('500');
		}
	});
});


$(function(){
	$(".accordian6_item_header").unbind();
	$('.accordian6_item:first-child').addClass('on').find('.accordian6_item_body').css('display','block');
	$('.js-accordian6_all .accordian6_item').addClass('on').find('.accordian6_item_body').css('display','block');
	$(".accordian6_item_header").on("click keypress",function(e){
		var $target = $(this).parent("li");
		if ((e.keyCode == 13)||(e.type == 'click'))$(this).parent("li").toggleClass("on").siblings().removeClass("on");
		if ( $target.is('.on')){
			$(this).next().slideDown('500');
			$target.siblings().find('.accordian6_item_body').slideUp('500');
		} else {
			$(this).next().slideUp('500');
		}
	});
});

