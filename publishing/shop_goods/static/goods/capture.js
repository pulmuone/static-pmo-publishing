// html2canvas(document.querySelector("#capture"), {
//   allowTaint: true,
//   taintTest: false,
//   useCORS: true,
// }).then(function (canvas) {
//   var imgageData = canvas.toDataURL("image/jpg");
//   var newData = imgageData.replace(/^data:image\/jpg/, "data:application/octet-stream");
//   jQuery("a").attr("download", "_상세.jpg").attr("href", newData);
// });


function partShot() {
  html2canvas(document.querySelector("#capture"), {
      logging: true,
      letterRendering: 1,
      allowTaint: true,
      taintTest: false,
      useCORS: true,
    })
    .then(canvas => {
      saveImg(canvas.toDataURL('image/jpeg', 1.0), "외부몰상세.jpg");
    }).catch(function(err) {
      console.log(err);
    });
}

function saveImg(uri, filename) {
  var link = document.createElement('a');
  if (typeof link.download === 'string') {
    link.href = uri;
    link.download = filename
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  } else {
    window.open(uri);
  }
}