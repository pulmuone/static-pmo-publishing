## 1. 기본 셋팅
### 1-1. 사용 라이브러리
- [jquery 3.6.0](https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js)

### 1-2. Vscode 플러그인
#### ● Prettier - Code formatter


## 2. 폴더 구조
| 폴더명  | 설명 | 
| --- | --- | 
| `legacy` | 이 전에 사용된 레거시 파일 |
| `mail` | 메일링 관련 (폴더 네이밍 : '연도 > 오픈일' 형식) |
| `shop_goods` | 상품 상세페이지 관련 (폴더 네이밍 :'상품번호_상품명' 형식) |
| `shop_pulmuone` | 이벤트, 프로모션 관련 (planshop 폴더 내 네이밍 : '오픈연월 > 오픈월일_이벤트명' 형식)<br/> css 폴더 내 파일 설명 : font.css(폰트 관련) / pc.css, mobile.css(프론트 관련) / p_common.css, m_common.css(이벤트, 상세페이지 내 공통 컴포넌트)|


## 3. 이미지 업로드
###  3-1. 이미지 업로드 방식
기획전 이미지 : jpg 형식으로 quality: 70~80% 권장<br/>
상품상세 이미지 : jpg, webp 확장자로 이미지 저장 권장

### 3-2. 이미지 ftp 업로드 폴더 경로
이미지는 '절대경로'로 작업합니다.

●  상품상세 경로
/prod_data/image/newProduct21 <br/> <br/>
● 이벤트/기획전 경로
/prod_data/image/MALL/planshop <br/> <br/>
● 메일링 경로
/prod_data/image/MALL/mail/


## 4. index 및 컴포넌트, 폰트 관련 사항
### 4-1. index 페이지 (유관부서 공유용)
메인 이벤트 : http://static.pulmuone.info/shop_pulmuone/main_index.html <br/>
이벤트 : http://static.pulmuone.info/shop_pulmuone/index.html <br/>
기획전 : http://static.pulmuone.info/shop_pulmuone/planshop_index.html <br/>
메일링 : http://static.pulmuone.info/shop_pulmuone/mailing.html <br/>
상품상세 : BOS 내 메뉴 '품목/상품관리' > '마스터 품목 콘텐츠 리스트'

### 4-2. 컴포넌트 샘플 페이지 (작업 참고용)
● 기획전, 이벤트 샘플 페이지 <br/>
PC 버전<br/>
http://static.pulmuone.info/shop_pulmuone/planshop/static/component_2024/p_evt_01.html<br/><br/>
MOBILE 버전<br/>
http://static.pulmuone.info/shop_pulmuone/planshop/static/component_2024/m_evt_01.html

● 상세 페이지 샘플  페이지<br/>
20240517 상품상세 에디터 기본 템플릿 (2024년 리뉴얼버전)<br/>
http://static.pulmuone.info/shop_goods/etc.html

### 4-3. 폰트 관련
기본 폰트 Pretendard체 사용 font-weight 200(ExtraLight), 400(Regular), 600(SemiBold), 800(ExtraBold) 사용

## 5. CSS 속성 순서
CSS는 공백없이 한줄로 작성합니다. <br/>
(코드의 통일성을 위해 순서 꼭 준수해서 작업 바랍니다.)<br/>
1 레이아웃 : position, top, right, bottom, left, display, flex-direction, align-items,float,  overflow, clear, , z-index<br/> 
2 BOX : width, height, margin, padding, border<br/>
3 배경 : background<br/>
4 폰트 : font,color, letter-spacing, text-align, text-decoration, text-indent, verticalalign, white-space<br/>
5 기타 위에 언급되지 않은 나머지 속성

| 속성명 |  |  |  |
| --- | --- |  --- |  --- | 
| position  | 
| top | 
| right | 
| bottom | 
| left | 
| display | 
| flex-direction |  
| overflow | 
| clip | 
| z-index |
| width | 
| min-width | 
| max-width | 
| height | 
| min-height | 
| max-height | 
| box-sizing | 
| margin | 
| margin-top | 
| margin-right | 
| margin-left | 
| margin-bottom | 
| padding | 
| padding-top | 
| padding-right | 
| padding-left | 
| padding-bottom | 
| border | 
| background-color | 
| background-image | 
| font-family | 
| font-size | 
| color | 
| line-height | 
| letter-spacing | 
| text-align | 
| white-space | 

## 6. 이벤트 / 기획전 업무 관련
###  6-1. 이미지 및 파일 경로
기본적으로 cdn 경로 사용('https://shoppulmuone.cdn.ntruss.com/MALL/' 로 시작됩니다.) <br/> <br/>
예시 경로) <br/>
img src="https://shoppulmuone.cdn.ntruss.com/MALL/planshop/202407/0716_sample.jpg" alt=""

###  6-2. CSS, JS 파일 경로
기본적으로 cdn 경로 사용('https://shoppulmuone.cdn.ntruss.com/MALL/' 로 시작됩니다.) <br/>
오픈 후, 수정되는 경우 경로 끝에 <span style="color:#74B827">'?버전=날짜' 형식</span>으로 쿼리스트링을 추가합니다.<br/> <br/>

오픈 전 작업할 경우 ▶️ 쿼리 스트링 없음 <br/>
예시 경로) <br/>
link rel="stylesheet" type="text/css" href="https://shoppulmuone.cdn.ntruss.com/MALL/planshop/202407/0716_sample/css/p_evt.css" <br/>

오픈 후 기획전, 이벤트가 수정할 경우 ▶️ 쿼리 스트링 추가<br/>
예시 경로) <br/>
link rel="stylesheet" type="text/css" href="https://shoppulmuone.cdn.ntruss.com/MALL/planshop/202407/0716_sample/css/p_evt.css?v1=20240716"<br/>
script type="text/javascript" src="https://shoppulmuone.cdn.ntruss.com/MALL/planshop/202407/0715_renewal_open_member/js/slide_verpc2.js?v1=20240716"

### 6-3. CSS 클래스명 작명
기본 규칙 : _(언더바) 사용 <br/>
<p style="color:#74B827">주의사항 : 클래스명에 'ad'를 넣으면 광고 막아주는 플러그인에서 자동으로 display:none; 처리됨.</p>

## 7. 지원 브라우저
ie 11, chrome, Microsoft Edge


## 8. 작업 브랜치 관련
● master 브랜치 : 이벤트, 기획전 작업 <br/>
● shop_goods 브랜치 : 상품상세 작업 <br/>
● feature 관련 브랜치 : 개발자분들이 사용하시는 브랜치 입니다. (참고용)


